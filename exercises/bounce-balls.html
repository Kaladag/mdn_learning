<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Bouncing balls</title>
    <style>
        html, body {
            margin: 0;
        }

        html {
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            height: 100%;
        }

        body {
            overflow: hidden;
            height: inherit;
        }

        h1 {
            font-size: 2rem;
            letter-spacing: -1px;
            position: absolute;
            margin: 0;
            top: -4px;
            right: 5px;

            color: transparent;
            text-shadow: 0 0 4px white;
        }

        p {
            position: absolute;
            margin: 0;
            top: 35px;
            right: 5px;
            color: #aaa;
        }
    </style>
</head>

<body>
    <h1>bouncing balls</h1>
    <p>Ball count: </p>
    <canvas></canvas>

    <script>
        // define variable for ball count paragraph
        const para = document.querySelector('p');
        let count = 0;

        // setup canvas
        const canvas = document.querySelector('canvas');
        // constant (ctx) is the object that directly represents the drawing area of the canvas
        //      and allows us to draw 2D shapes on it
        const ctx = canvas.getContext('2d');

        // width and height of the browser viewport (the area that the webpage appears on —
        //      this can be gotten from the Window.innerWidth and Window.innerHeight properties)
        // we are chaining multiple assignments together, to get the variables all set quicker — this is perfectly OK
        const width = canvas.width = window.innerWidth;
        const height = canvas.height = window.innerHeight;

        // function to generate random number
        function random(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        // define shape constructor
        function Shape(x, y, velX, velY, exists) {
            // x and y coordinates — the horizontal and vertical coordinates where the ball starts on the screen
            // this can range between 0 (top left hand corner) to the width and height of the browser viewport
            //      (bottom right hand corner)
            this.x = x;
            this.y = y;

            // horizontal and vertical velocity (velX and velY) — each ball is given a horizontal and vertical velocity;
            // in real terms these values are regularly added to the x/y coordinate values when we animate the balls,
            //      to move them by this much on each frame
            this.velX = velX;
            this.velY = velY;

            // to track whether the balls exist in the program (have not been eaten by the evil circle)
            this.exists = exists;
        }


        // define Ball constructor, inheriting from Shape
        function Ball(x, y, velX, velY, exists, color, size) {
            Shape.call(this, x, y, velX, velY, exists);

            // ball color
            this.color = color;

            // ball radius in px
            this.size = size;
        }

        // correction of Ball's prototype and constructor (as explained in class inheritance)
        Ball.prototype = Object.create(Shape.prototype);
        Ball.prototype.constructor = Ball;

        // Drawing the ball

        // using this function, we can tell the ball to draw itself onto the screen, by calling a series of members
        //      of the 2D canvas context we defined earlier (ctx)
        // the context is like the paper, and now we want to command our pen to draw something on it:
        Ball.prototype.draw = function() {
            //  to state that we want to draw a shape on the paper
            ctx.beginPath();

            // to define what color we want the shape to be - we set it to our ball's color property
            ctx.fillStyle = this.color;

            // to trace an arc shape on the paper
            // last two parameters specify the start and end number of degrees around the circle that the arc is drawn between
            //      2 * PI is the equivalent of 360 degrees in radians (only 1 * PI, you'd get a semi-circle (180 degrees))
            ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);

            // finish drawing the path we started with beginPath(), and fill the area it takes up with the color we
            //      specified earlier in fillStyle
            ctx.fill();
        }

        // Updating the ball's data
        Ball.prototype.update = function() {

            // check whether the ball has reached the edge of the canvas
            // if it has, we reverse the polarity of the relevant velocity to make the ball travel in the opposite direction
            // in each case, we include the size of the ball in the calculation because the x/y coordinates are
            //      in the center of the ball, but we want the edge of the ball to bounce off the perimeter —
            //      we don't want the ball to go halfway off the screen before it starts to bounce back
            if ((this.x + this.size) >= width) {
                // the ball is going off the right edge
                this.velX = -(this.velX);
            }

            if ((this.x - this.size) <= 0) {
                // the ball is going off the left edge
                this.velX = -(this.velX);
            }

            if ((this.y + this.size) >= height) {
                // the ball is going off the bottom edge
                this.velY = -(this.velY);
            }

            if ((this.y - this.size) <= 0) {
                // the ball is going off the top edge
                this.velY = -(this.velY);
            }

            // the ball is in effect moved each time this method is called
            this.x += this.velX;
            this.y += this.velY;
        }

        // Adding collision detection
        //      add some collision detection to our program, so our balls know when they have hit another ball

        Ball.prototype.collisionDetect = function() {

            // for each ball, we need to check every other ball to see if it has collided with the current ball
            for (let j = 0; j < balls.length; j++) {

                // to check whether the current ball being looped through is the same ball as the one we are currently
                //      checking so that the code inside the if statement only runs if they are not the same - we don't
                //      want to check whether a ball has collided with itself
                // also a ball needs to be considered for collision detection only if the "exists" property is true
                if (!(this === balls[j])) {
                    // use a common algorithm to check the collision of two circles - we are basically checking
                    //      whether any of the two circle's areas overlap
                    const dx = this.x - balls[j].x;
                    const dy = this.y - balls[j].y;
                    const distance = Math.sqrt(dx * dx + dy * dy);

                    // if a collision is detected
                    if (distance < this.size + balls[j].size && balls[j].exists) {
                        // we only set the color property of both the circles to a new random color
                        balls[j].color = this.color = 'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) +')';
                    }
                }
            }
        }

        // define EvilCircle constructor, inheriting from Shape
        function EvilCircle(x, y, exists) {
            // velX and velY should always equal 20
            Shape.call(this, x, y, 20, 20, exists);

            // EvilCircle color
            this.color = 'white';

            // EvilCircle radius in px
            this.size = 10;
        }

        // correction of EvilCircle's prototype and constructor (as explained in class inheritance)
        EvilCircle.prototype = Object.create(Shape.prototype);
        EvilCircle.prototype.constructor = EvilCircle;

        // Drawing the EvilCircle
        EvilCircle.prototype.draw = function() {
            //  to state that we want to draw a shape on the paper
            ctx.beginPath();

            // to define what color we want the shape to be - we set it to our EvilCircle's color property
            // we want the evil circle to not be filled in, but rather just have an outer line (stroke)
            ctx.strokeStyle = this.color;

            // also want to make the stroke a bit thicker
            ctx.lineWidth = 3;

            // to trace an arc shape on the paper
            // last two parameters specify the start and end number of degrees around the circle that the arc is drawn between
            //      2 * PI is the equivalent of 360 degrees in radians (only 1 * PI, you'd get a semi-circle (180 degrees))
            ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);

            // finish drawing the path we started with beginPath(), and stroke the area it takes up with the color we
            //      specified earlier in strokeStyle
            ctx.stroke();
        }

        // Stop circle disappearing from screen
        // look to see whether the evil circle is going to go off the edge of the screen, and stop it from doing so
        EvilCircle.prototype.checkBounds = function() {
            if((this.x + this.size) >= width) {
                this.x -= this.size;
            }

            if((this.x - this.size) <= 0) {
                this.x += this.size;
            }

            if((this.y + this.size) >= height) {
                this.y -= this.size;
            }

            if((this.y - this.size) <= 0) {
                this.y += this.size;
            }
        }

        // Moving circle around
        // will add an onkeydown event listener to the window object so that when certain keyboard keys are pressed,
        //      we can move the evil circle around
        EvilCircle.prototype.setControls = function() {
            let _this = this;
            window.onkeydown = function(e) {
                if (e.key === 'a') {
                    _this.x -= _this.velX;
                } else if (e.key === 'd') {
                    _this.x += _this.velX;
                } else if (e.key === 'w') {
                    _this.y -= _this.velY;
                } else if (e.key === 's') {
                    _this.y += _this.velY;
                }
            }
        }

        // Adding collision with balls
        EvilCircle.prototype.collisionDetect = function() {

            for (let j = 0; j < balls.length; j++) {

                // need to do a test to see if the ball being checked exists
                //      if it doesn't exist, it has already been eaten by the evil circle, so there is no need to check it again
                if (balls[j].exists) {

                    // use a common algorithm to check the collision of two circles - we are basically checking
                    //      whether any of the two circle's areas overlap
                    const dx = this.x - balls[j].x;
                    const dy = this.y - balls[j].y;
                    const distance = Math.sqrt(dx * dx + dy * dy);

                    // if a collision is detected
                    if (distance < this.size + balls[j].size) {
                        // want to set any balls that collide with the evil circle to not exist any more
                        balls[j].exists = false;

                        // update the counter of remaining balls
                        count--;
                        para.textContent = 'Ball count: '+ count;
                    }
                }
            }
        }


        // Animating the ball
        let balls = [];

        while (balls.length < 25) {
            let size = random(10,20);
            let ball = new Ball(
                // ball position always drawn at least one ball width away from the edge of the canvas, to avoid drawing errors
                random(0 + size, width - size),
                random(0 + size, height - size),
                random(-7, 7),
                random(-7, 7),
                true,
                'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) +')',
                size,
            );

            balls.push(ball);

            // update the counter with new balls
            count++;
            para.textContent = 'Ball count: ' + count;
        }

        // Define loop that keeps drawing the scene constantly

        let evil = new EvilCircle(random(0,width), random(0,height), true);
        evil.setControls();

        // all programs that animate things generally involve an animation loop, which serves to update the information
        //      in the program and then render the resulting view on each frame of the animation
        function loop() {
            // sets the canvas fill color to semi-transparent black to allow the previous few frames to shine through slightly,
            //      producing the little trails behind the balls as they move
            ctx.fillStyle = 'rgba(0, 0, 0, 0.25)'; // if you changed 0.25 to 1, you won't see them at all any more
            // draws a rectangle of the color across the whole width and height of the canvas
            ctx.fillRect(0, 0, width, height);
            // -> these both serve to cover up the previous frame's drawing before the next one is drawn
            // (if you don't do this, you'll just see long snakes worming their way around the canvas instead of balls moving)

            // to draw each ball on the screen, then do the necessary updates to position and velocity in time for the next frame
            for (let i = 0; i < balls.length; i++) {
                if(balls[i].exists) {
                    balls[i].draw();
                    balls[i].update();
                    balls[i].collisionDetect();
                }
            }

            evil.draw();
            evil.checkBounds();
            evil.collisionDetect();

            // runs the function again
            // when this method is repeatedly run and passed the same function name, it runs that function a set number
            //      of times per second to create a smooth animation
            // this is generally done recursively — which means that the function is calling itself every time it runs,
            //      so it runs over and over again
            requestAnimationFrame(loop);
        }

        // call the function once to get the animation started
        loop();

    </script>
</body>
</html>