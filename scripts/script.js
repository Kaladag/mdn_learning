//*************** LEARNING POINTS ***************
// this file contains learning knowledge of JS

/*
|| API
- nadstavnou JS, kterou muzeme vyuzivat, jsou APIs (Application Programming Interfaces);
2 kategorie:
    - Browser APIs:
        - are able to expose data from the surrounding computer environment, or do useful complex things;
        - DOM (Document Object Model) API: allows you to manipulate HTML and CSS;
        - Geolocation API: is how Google Maps is able to find your location and plot it on a map;
        - Canvas and WebGL APIs: allow you to create animated 2D and 3D graphics;
        - Audio and Video APIs (like HTMLMediaElement and WebRTC): allow you to do really interesting things with multimedia;
    - Third party APIs:
        - are not built into the browser by default, and you generally have to grab their code and information
            from somewhere on the Web;
        -  Twitter API,  Google Maps API,...;


|| WHAT JS IS AND WHAT DOES ON PAGE
- When you load a web page in your browser, you are running your code (the HTML, CSS, and JavaScript) inside
    an execution environment (the browser tab);
- Each browser tab has its own separate bucket for running code in (these buckets are called "execution environments"
    in technical terms);
- this means that in most cases the code in each tab is run completely separately, and the code in one tab cannot
    directly affect the code in another tab;
(There are ways to send code and data between different websites/tabs in a safe manner, but these are advanced techniques);
- When the browser encounters a block of JavaScript, it generally runs it in order, from top to bottom;
- JS is case sensitive
- most things are "objects" in JS, e.g. when you create:
    let string = 'This is my string';
    - the variable becomes a string object instance, and as a result has a large number of properties and methods available to it;

/// INTERPRETED and COMPILED languages:
- INTERPRETED:
    - The code is run from top to bottom and the result of running the code is immediately returned;
    - You don't have to transform the code into a different form before the browser runs it;
- COMPILED:
    - are transformed (compiled) into another form before they are run by the computer, for example, C/C++ are compiled
        into machine code that is then run by the computer;
    - The program is executed from a binary format, which was generated from the original program source code;
- From a technical standpoint, most modern JS interpreters actually use a technique called "just-in-time compiling"
    to improve performance; the JS source code gets compiled into a faster, binary format while the script is being used,
    so that it can be run as quickly as possible;
- JavaScript is still considered an "interpreted language", since the compilation is handled at run time, rather
    than ahead of time;

/// SERVER-SIDE versus CLIENT-SIDE code:
- Client-side code is code that is run on the user's computer (when a web page is viewed, the page's client-side code
    is downloaded, then run and displayed by the browser);
- Server-side code on the other hand is run on the server, then its results are downloaded and displayed in the browser;
    - e.g. PHP, Python, Ruby, ASP.NET,... and JS in the popular Node.js;

/// DYNAMIC VS. STATIC
- static — no dynamically updating content; it just shows the same content all the time;
- dynamic - is used to describe both client-side JavaScript, and server-side languages — it refers to the ability
to update the display of a web page/app to show different things in different circumstances, generating new content as required;

// ADDING JS TO A PAGE
- INTERNAL JS
    - putting a JS code into <script> element into the end of a <head> element of HTML file;
    document.addEventListener("DOMContentLoaded", function() {
  ...
    });
- EXTERNAL JS
    - putting JS file into "src" attribute of <script> element in a <head> element of HTML file;
- INLINE JS HANDLERS
    - <button onclick="createParagraph()">Click me!</button> and JS code put under this in <script> element
    - Please don't do this, however. It is bad practice to pollute your HTML with JS, and it is inefficient —
        you'd have to include the onclick="createParagraph()" attribute on every button you want the JS to apply to;

/// SCRIPT LOADING STRATEGIES
- code won't work if the JS is loaded and parsed before the HTML you are trying to do something to;
- in the internal example above we used an event listener, which listens for the browser's "DOMContentLoaded" event,
    which signifies that the HTML body is completely loaded and parsed;
    - The JS inside this block will not run until after that event is fired;
- In the external example, we use a more modern JavaScript feature - the "defer" attribute,
    which tells the browser to continue downloading the HTML content once the <script> tag element has been reached;
    - "defer" only works for external scripts;
- old-fashioned solution: put <script> element right at the bottom of the <body> (e.g. just before the </body> tag);
    - problem is that loading/parsing of the script is completely blocked until the HTML DOM has been loaded,
    and can cause a major performance issue, slowing down the site;

ASYNC attr. VS. DEFER attr.
- ASYNC:
    - will download the script without blocking the page while the script is being fetched;
    - once the download is complete, the script will execute, which blocks the page from rendering;
    - no guarantee that scripts will run in any specific order;
    - WHEN USE? - when the scripts in the page run independently from each other and depend on no other script on the page;
        - should be used when you have a bunch of background scripts to load in, and you just want to get them in place
        as soon as possible;
- DEFER:
    - scripts will load in the order they appear on the page (put their corresponding <script> elements in the order
        you want the browser to execute them);
    - They won't run until the page content has all loaded, which is useful if your scripts depend on the DOM being
        in place (e.g. they modify one or more elements on the page);
    - WHEN USE? - if your scripts need to wait for parsing and depend on other scripts and/or the DOM being in place;
- see picture "defer-vs-async-attr" in "img" file;


|| VARIABLES
- contain values - Variables aren't the values themselves, they are containers for values;
- To use a variable, you've first got to create it — more accurately, we call this "declaring the variable";
- Once you've declared a variable, you can initialize it with a value (assign a value to it with equals sign (=));
- POZOR: promenna muze obsahovat "hodnotu" anebo "odkaz" (objekt, funkci);

var
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/var
    (= 'variable');
     - "var" declarations, wherever they occur, are processed before any code is executed - this is called "hoisting"
       ( = declaring a variable anywhere in the code is equivalent to declaring it at the top of the function or global code);
     - their initial value is undefined;
     - The scope of a variable declared with "var" is its current execution context and closures thereof,
        which is either the enclosing function and functions declared within it, or, for variables declared outside
        any function, global, other words declares a variable globally, or locally to an entire function regardless of block scope;
    - duplicate variable declarations using "var" will not trigger an error, even in strict mode, and the variable
        will not lose its value, unless another assignment is performed;
    - hoisting will affect the variable declaration, but not its value's initialization - the value will be indeed
        assigned when the assignment statement is reached:
            var x = y, y = 'A';
            console.log(x + y); // undefinedA
    - when you use "var", you can declare the same variable as many times as you like (var myName = 'Chris'; var myName = 'Bob';);
    - In the global context, a variable declared using "var" is added as a non-configurable property of the global object;

let
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let
    (= let "a" be 1 as classic mathematical statement);
    - allows you to declare variables that are limited to the scope of a block statement, or expression on which it is used;
    - Hoisting no longer works with "let";
    - just like "const" the "let" does not create properties of the "window" object when declared globally (in the top-most scope);
    - you cannot declare the same variable as many times as you like but you can assign a new value of course
        (let myName = 'Chris'; myName = 'Bob';);
    - SHOULD BE USED INSTEAD OF "VAR" unless for old versions of IE (supported from IE11);
    - At the top level of programs and functions, let, unlike var, does not create a property on the global object:
        var x = 'global';
        let y = 'global';
        console.log(this.x); // "global"
        console.log(this.y); // undefined
    - let variables cannot be read/written until they have been fully initialized, accessing the variable
        before the initialization results in a ReferenceError - the variable is said to be in a "temporal dead zone" (TDZ)
        from the start of the block until the initialization has completed;
        (temporal = dočasný, časový)
            { // TDZ starts at beginning of scope
              console.log(bar); // undefined
              console.log(foo); // ReferenceError
              var bar = 1;
              let foo = 2; // End of TDZ (for foo)
            }
        - The term "temporal" is used because the zone depends on the order of execution (time) rather than the order
            in which the code is written (position).
            {
                // TDZ starts at beginning of scope
                const func = () => console.log(letVar); // OK

                // Within the TDZ letVar access throws `ReferenceError`

                let letVar = 3; // End of TDZ (for letVar)
                func(); // Called outside TDZ!
            }

const
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const#description
    (= constant);
    - lets us store values that can never be changed;
        - an initializer for a constant is required - you must specify its value in the same statement in which it's declared
        (This makes sense, given that it can't be changed later.);
    - scope can be either global or local to the block in which it is declared;
    - Global constants do not become properties of the window object, unlike var variables;
    - const declaration creates a read-only reference to a value (to neznamenna, ze hodnota je nemenna, pouze to, ze
        identifikator nelze znovu priradit, napr. hodnoty propert v objektu, ktery je prirazen k promenne const, mohou
        byt aktualizovany; aby se tomuto zabranilo, pouziva se "Object.freeze()" to make object immutable, naproti tomu,
        keys meneny byt nemuzou; podobne je to u arrays - je mozne "push"ovat hodnoty do pole, ale priradit nove pole
        k dane const jiz nejde);
    - All the considerations about the "temporal dead zone" apply to const too;
    - constant cannot share its name with a function or a variable in the same scope; outside a scope it is possible
        in some cases like:

        const MY_FAV = 7;

        if (MY_FAV === 7) {
          let MY_FAV = 20; // this is fine and creates a block scoped MY_FAV variable (works equally well
                              with let to declare a block scoped non const variable)
          console.log('my favorite number is ' + MY_FAV); // MY_FAV is now 20
          var MY_FAV = 20; // this gets hoisted into the global context and throws an error
        }

        console.log('my favorite number is ' + MY_FAV); // MY_FAV is still 7
    - Constants can be declared with uppercase or lowercase, but a common convention is to use all-uppercase letters;


/// NAMING VARIABLES
- just using Latin characters (0-9, a-z, A-Z) and the underscore character;
- Don't use underscores, capitals and numbers at the start of variable names;
- A safe convention to stick to is so-called "lower camel case";
- Make variable names intuitive, so they describe the data they contain. Don't just use single letters/numbers, or big long phrases;
- Variables are case sensitive;
- also need to avoid using JS reserved words as your variable names (e.g. var, function, let, for, ...);
    - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#keywords


/// VARIABLE TYPES
- Numbers
    - 30 (also called "integers") or decimal numbers like 2.456 (also called "floats" or "floating point numbers");
- Strings
    - are pieces of text that you need to wrap in single or double quote marks;
- Booleans
    - are true/false values;
    - using the "less than" operator (<) or "greater than" (>);
- Arrays
    - a single object that contains multiple values enclosed in square brackets and separated by commas;
    - arrays in JS are zero-indexed: the first element is at index 0;
- Objects

/// DYNAMIC TYPING
- js is a "dynamically typed language", which means that, unlike some other languages, you don't need
    to specify what data type a variable will contain (numbers, strings, arrays, etc);
    - the browser treats the variable type;


|| BASIC MATH IN JS - NUMBERS AND OPERATORS
- types of decimal numbers:
    - Integers
        = whole numbers, e.g. 10, 400, or -5;
    - Floating point numbers (floats)
        - have decimal points and decimal places, for example 12.5, and 56.7786543;
    - Doubles
        = a specific type of floating point number that have greater precision than standard floating point numbers
        (meaning that they are accurate to a greater number of decimal places);
- types of number systems:
    - Decimal
        - base 10 (meaning it uses 0–9 in each column;
    - Binary
        — The lowest level language of computers; 0s and 1s;
    - Octal
        — Base 8, uses 0–7 in each column;
    - Hexadecimal
        — Base 16, uses 0–9 and then a–f in each column;
- JS only has one data type for numbers, both integers and decimals: "Number" (= The Number object);
    (actually, JS has a second number type: "BigInt" used for very, very large integers);

/// NUMBER METHODS
- toFixed()
    - round your number to a fixed number of decimal places;
    let lotsOfDecimal = 1.766584958675746364;
    let twoDecimalPlaces = lotsOfDecimal.toFixed(2); // 1.77

/// Converting to number data types
- operator called "typeof" shows the data type of variable:
    typeof myInt;
- converting is done by passing the string value into the Number() constructor to return a number version of the same value:
    let myNumber = '74';
    Number(myNumber);

/// ARITHMETIC OPERATORS
- Addition: + (6 + 9);
- Subtraction - (9 - 6);
- Multiplication * (6 * 9);
- Division / (9 / 3);
- Remainder (sometimes called Modulo) % (8 % 3 = 2) = je to zbytek po deleni cisla;
- Exponent ** (5 ** 2 = 25; 5 ** 3 = 125) (valid from ES7); older method is Math.pow(): Math.pow(5, 3) = 5 ** 3;

- Operator precedence in JS is the same as is taught in math (Modulo has precedence before Multiplication/Division);
- numbers in operations are referred as operand (operand is the part of an instruction representing the data
    manipulated by the operator, e.g. when you add two numbers, the numbers are the operand and "+" is the operator);

/// Increment and decrement OPERATORS
- repeatedly add or subtract one to or from a numeric variable value;
    incCount++;
    decCount--;
- you can't apply these directly to a number, only to variable;
- browser returns the current value, then increments the variable; we can make the browser do it the other way round —
    increment/decrement the variable then return the value — by putting the operator at the start of the variable
    instead of the end: ++incCount;

/// Assignment OPERATORS
- assign a value to a variable
- the assignment statements are considered from right to left:
let y,z,p,q;
q=200;
y=z=p=q;

y===200 // true
z===200 // true
p===200 // true
q===200 // true

** Augmented assignment OPERATORS
- shortcut operators like "+=" (name += ' says hello!' is the same as name = name + ' says hello!') called Addition assignment;
- next are e.g.: -=; *=; /=;
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#assignment_operators

/// Comparison OPERATORS
- if we want to run true/false tests;
- Strict equality ===
- Strict-non-equality !==
- Less than <
- Greater than >
- Less than or equal to <=
- Greater than or equal to >=
- sometimes used: == or !=; they test whether the values are the same but not whether the values' data-types are the same;
    - they tend to result in errors so not recommended for use;
- equality operators are often being used for control that swaps between two states generally referred to as a "toggle";


|| HANDLING TEXT - STRINGS IN JS
- Strings can be created as "primitives", from string literals, or as "objects", using the String() constructor:
    const string1 = "A string primitive";
    const string2 = new String("A String object");

/// ESCAPING CHARACTERS IN A STRING
- we do this by putting a backslash just before the character to make sure they are recognized as text, not part of the code;
    let bigmouth = 'I\'ve got no right to take my place...';

/// CONCATENATING STRINGS
- when you enter an actual string in your code, enclosed in single or double quotes, it is called a string literal;
    let response = one + 'I am fine — ' + two;

/// CONVERTING TO STRINGS
- if we have a numeric variable that we want to convert to a string but not change otherwise, every number has
    a method called toString() that converts it to the equivalent string;
    let myNum = 123;
    let myString = myNum.toString();

/// TEMPLATE LITERALS
- another type of string syntax, sometimes referred to as "template strings";
- we use backtick characters for the strings;
    song = `Fight the Youth`;
- easier escaping characters, variables are put inside a ${ } construct, which is called a "placeholder":
    output = `I like the song "${ song }". I gave it a score of ${ score/highestScore * 100 }%.`;
- in the past when we want to split a traditional string over multiple lines (for printing to console),
    we need to include a newline character, \n:
    output = 'I like the song "' + song + '".\nI gave it a score of ' + (score/highestScore * 100) + '%.';
    - but now template literals respect the line breaks in the source code, so newline characters are no longer needed:
    output = `I like the song "${ song }".
    I gave it a score of ${ score/highestScore * 100 }%.`;
- to escape a backtick in a template literal, put a backslash (\) before the backtick;


/// STRINGS AS OBJECTS
- when create variable from string literals it becomes a "string object instance" and as a result has
    a large number of properties and methods available to it (due to "String object");
-
** LENGTH
    let browserType = 'mozilla';
    browserType.length;

** SUBSTRING INSIDE A STRING
-- indexOf()
    - sometimes you'll want to find if a smaller string is present inside a larger one (we generally say "if a substring
        is present inside a string") - this can be done using the "indexOf()" method;
        browserType.indexOf('zilla'); // 2
        browserType.indexOf('vanilla'); // -1 is returned when the substring is not found in the main string

-- slice()
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/slice
    - the first parameter is the character position to start extracting at, and the second parameter is the character
    position after the last one to be extracted - the slice happens from the first position, up to, but not including,
    the last position;
    - if beginIndex or endIndex is negative, slice() is treated as str.length + beginIndex/endIndex;
    browserType.slice(0,3); // "moz"
    browserType.slice(2); // "zilla" - if you want to extract all of the remaining characters in a string
        after a certain character, you don't have to include the second parameter;
    browserType.slice(-2); // "la"

** CHANGING CASE
    let radData = 'My NaMe Is MuD';
    radData.toLowerCase(); // "my name is mud"
    radData.toUpperCase(); // "MY NAME IS MUD"

** UPDATING PARTS OF A STRING
-- replace()
    - it takes two parameters — the string you want to replace, and the string you want to replace it with;
    - it doesn't just update the substring value automatically:
    browserType.replace('moz','van'); // "vanilla" but value of browserType is still "mozzila"
    - to update value of variable we need to store it in the variable itself:
    browserType = browserType.replace('moz','van');


|| ARRAYS
- are generally described as "list-like objects";
- JS "Array" class is a global object that is used in the construction of arrays; which are high-level, list-like objects;
- they are basically single objects that contain multiple values stored in a list;
- in an array we can store various data types — strings, numbers, objects, and even other arrays;
- we can also mix data types in a single array;
    let shopping = ['bread', 'milk', 'cheese', 'hummus', 'noodles'];
    let sequence = [1, 1, 2, 3, 5, 8, 13];
    let random = ['tree', 795, [0, 1, 2]];
- an array inside an array is called a "multidimensional array";
- indexy jsou vzdy integers; muzeme pomoci bracket notation vytvorit i index s prirazenou hodnotou, ktery bude treba string,
    ale na takovy element pole se pak nevztahuji traversal and mutation operations
    (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Indexed_collections#array_methods)
    shopping['polozka'] = 'butter'; //
        0: "bread"
        1: "milk"
        2: "cheese"
        3: "hummus"
        4: "noodles"
        polozka: "butter"
        length: 5

/// ACCESSING AND MODIFYING ARRAY ITEMS
    shopping[0]; // "bread"
    shopping.0; // a syntax error
        - JS properties that begin with a digit cannot be referenced with dot notation and must be accessed
            using bracket notation;
    shopping[shopping.length - 1]; // 'noodles'
    shopping[0] = 'tahini'; // shopping will now return [ "tahini", "milk", "cheese", "hummus", "noodles" ]
- accessing item in multidimensional array by chaining two sets of square brackets together:
    random[2][2]; // 2
- The index number is coerced into a string by the JS engine through an implicit "toString" conversion, due to it is
    not necessary to access element of array through shopping['0'];
    - also '2' and '02' would refer to two different slots on the shopping object;

** LENGTH
- works as the same way as for strings
    shopping.length;
- it is most commonly used to tell a loop to keep going until it has looped through all the items in an array;
- JS array's length property and numerical properties are connected - when some  built-in array methods are called, they
    take into account the value of an array's length property;

/// METHODS useful using in ARRAY
- jak jsme poznamenali vyse, pole je automaticky instanci Array tridy a proto ma pristup k propertama metodam Array tridy -
    pristupovat k nim muzeme bud diky "dot notation" (object.property_name) nebo "bracket notation" (object[property_name]);
    - pro property a metody objektu je vice pouzivana "dot notation";

** split()
- is technically a string method, not an array method but works well for arrays too;
- takes a single parameter, the character you want to separate the string at, and returns the substrings
    between the separator as items in an array;
    let myData = 'Manchester,London,Liverpool,Birmingham,Leeds,Carlisle';
    let myArray = myData.split(',');
    myArray; // ["Manchester", "London", "Liverpool", "Birmingham", "Leeds", "Carlisle"]
    myArray[myArray.length-1]; // the last item in the array "Carlisle"

** join()
- opposite way of split();
    let myNewString = myArray.join('&');
    myNewString; // "Manchester&London&Liverpool&Birmingham&Leeds&Carlisle"

** toString()
- as the same result as with join() w/o specification of separator parameter - it always uses a comma;
    let dogNames = ['Rocket','Flash','Bella','Slugger'];
    dogNames.toString(); // Rocket,Flash,Bella,Slugger

** push()
- adds an item at the end of an array;
    myArray.push('Cardiff'); // returns new length of the array when the method call completes
    myArray; // ["Manchester", "London", "Liverpool", "Birmingham", "Leeds", "Carlisle", "Cardiff"]

** pop()
- removes last item from the array;
    myArray.pop(); // returns last item of the array when the method call completes

** unshift()
- adds an item at the beginning of an array;
    myArray.unshift('Edinburgh', 'Maine');
    myArray; // ["Edinburgh", "Maine", "Manchester", "London", "Liverpool", "Birmingham", "Leeds", "Carlisle", "Cardiff"]

** shift()
- removes first item from the array;
    myArray.shift(); // returns first item of the array when the method call completes

** splice()
- changes the contents of an array by removing or replacing existing elements and/or adding new elements in place;
- return value is an array with affected elements; if no element is affected, empty array is returned;
- syntax:
splice(start)
splice(start, deleteCount)
splice(start, deleteCount, item1)
splice(start, deleteCount, item1, item2, itemN)

let myArray = ["Edinburgh", "Maine", "Manchester", "London", "Liverpool", "Birmingham", "Leeds", "Carlisle", "Cardiff"]
    - odstrani vse od start pozice (= index elementu) vcetne:
        myArray.splice(6) // return ["Leeds", "Carlisle", "Cardiff"];

    - pokud je start zaporny, pocita se index: array.length + start, v prikladu nize tedy 9 - 2 = 7;
    - deleteCount je 1, takze odstrani pouze 1 element na dane pozici
        myArray.splice(-2, 1) // return ["Carlisle"]

    - odstrani 2 elementy pocinaje pozici 0 a misto nich prida 2 nove elementy:
         myArray.splice(0, 1, 'Praha', 'Brno') // return ["Edinburgh"]
        ["Praha", "Brno", "Manchester", "London", "Liverpool", "Birmingham", "Leeds", "Carlisle", "Cardiff"]

** forEach()
    let fruits = ['Apple', 'Banana'];
    fruits.forEach(function(item, index, array) {
      console.log(item, index)
    });
    // Apple 0
    // Banana 1

** indexOf
let position = fruits.indexOf('Banana'); // 1

** slice()
- tato metoda bez pouziti parametru muze kopirovat cele pole;
    let shallowCopy = fruits.slice();


|| CONDITIONS - Conditional statements

/// IF ... ELSE
    if (condition) {
      code to run if condition is true
    } else {
      run some other code instead
    }

- we can use only IF w/o ELSE part but need to be careful when to use;
    if (condition) {
      code to run if condition is true
    }

- shorthand is also valid but not recommended as it is not well readable;
    if (condition) code to run if condition is true
    else run some other code instead

- for more conditions in one block of statement we use ELSE IF;
    if (condition) {
        code to run if condition is true
    } else if (another condition) {
        code to run if 1st condition is false
    } else {
        run some other code if none previous condition is true
    }

** FALSY (FALSEY) VALUE
- is a value that is considered false when encountered in a Boolean context;
if (false)
if (null) - the absence of any value
if (undefined) - the primitive value
if (0)
if (-0)
if (0n)
if (NaN) - not a number
if ("") / (''), (``) - empty string
- any value that is not 'false', 'undefined', 'null', '0', 'NaN', or an 'empty string ('')' actually returns 'true'
    when tested as a conditional statement;

    let shoppingDone = false;
    let childsAllowance;

    if (shoppingDone) { // don't need to explicitly specify '=== true'
      childsAllowance = 10;
    } else {
      childsAllowance = 5;
    }

- it is OK to put one if...else statement inside another one — to nest them;
- even though the code all works together, each if...else statement works completely independently of the other one;

** LOGICAL OPERATORS

-- && — AND:
    - allows you to chain together two or more expressions so that all of them have to individually
    evaluate to true for the whole expression to return true;
-- || — OR:
    - allows you to chain together two or more expressions so that one or more of them have to individually evaluate
     to true for the whole expression to return true;
-- ! — NOT:
    - can be used to negate an expression;

Logical operators in javascript, unlike operators in other programming languages, do not return true or false;
They always return one of the operands (based on truthy and falsy values);

OR ( || ) operator - If the first value is truthy, then the first value is returned;
    Otherwise, always the second value gets returned;

AND ( && ) operator - If both the values are truthy, always the second value is returned;
    If both the values are falsy, always the first value is returned;
    If the first value is falsy then the first value is returned or if the second value is falsy then
        the second value is returned;


/// SWITCH STATEMENT
- for cases where you just want to set a variable to a certain choice of value or print out a particular statement
    depending on a condition;

    switch (expression) {
      case choice1:
        run this code
        break;

      case choice2:
        run this code instead
        break;

      // include as many cases as you like

      default:
        actually, just run this code
    }

- you don't have to include the "default" section — you can safely omit it if there is no chance that the expression
    could end up equaling an unknown value;

/// TERNARY OPERATOR
- can be useful in some situations if you have two choices that are chosen between via a true/false condition;
    condition ? exprIfTrue : exprIfFalse
    ( condition ) ? run this code : run this code instead


|| LOOPS - Looping code
- programming loops are all to do with doing the same thing over and over again, which is termed "iteration" in programming speak;
- loop usually has one or more of the following features:
    - counter: is initialized with a certain value — this is the starting point of the loop;
    - condition: is a true/false test to determine whether the loop continues to run, or stops —
        usually when the counter reaches a certain value;
    - iterator: generally increments the counter by a small amount on each successive loop until the condition is no longer true;

/// FOR LOOP
    for (initializer; condition; final-expression) {
      // code to run
    }
-  initializer: is usually a variable set to a number, which is incremented to count the number of times
    the loop has run; it is also sometimes referred to as a counter variable;
- condition: as mentioned before, this defines when the loop should stop looping, this is generally an expression
    featuring a comparison operator, a test to see if the exit condition has been met;
- final-expression: is always evaluated (or run) each time the loop has gone through a full iteration;
    - it usually serves to increment (or in some cases decrement) the counter variable, to bring it closer to the point
        where the condition is no longer true;
- curly braces: contain a block of code — this code will be run each time the loop iterates;

const cats = ['Bill', 'Jeff', 'Pete', 'Biggles', 'Jasmin'];
let info = 'My cats are called ';
const para = document.querySelector('p');

for (let i = 0; i < cats.length; i++) {
  info += cats[i] + ', ';
}
para.textContent = info;
// output: My cats are called Bill, Jeff, Pete, Biggles, Jasmin,

- we must make sure the loop reaches the point where the condition is not true - if not, the loop will go on forever,
    and either the browser will force it to stop, or it will crash; this is called an 'infinite loop';

https://www.impressivewebs.com/javascript-for-loop/

** BREAK
- using "break" you can exit a loop before all the iterations have been completed, e.g if we want just find a concrete
    data - when found we can finish a loop and move on;

EXAMPLE
<label for="search">Search by contact name: </label>
<input id="search" type="text">
<button>Search</button>
<p></p>

const contacts = ['Chris:2232322', 'Sarah:3453456', 'Bill:7654322', 'Mary:9998769', 'Dianne:9384975'];
const para = document.querySelector('p');
const input = document.querySelector('input');
const btn = document.querySelector('button');

btn.addEventListener('click', function() {
  let searchName = input.value.toLowerCase();
  input.value = '';
  input.focus();
  for (let i = 0; i < contacts.length; i++) {
    let splitContact = contacts[i].split(':');
    if (splitContact[0].toLowerCase() === searchName) {
      para.textContent = splitContact[0] + '\'s number is ' + splitContact[1] + '.';
      break;
    } else if (i === contacts.length - 1) {
      para.textContent = 'Contact not found.';
    }
  }
});

** CONTINUE
- "continue" statement works in a similar manner to "break", but instead of breaking out of the loop entirely,
    it skips to the next iteration of the loop;

/// FOR...IN
    for (variable in object)
      statement
- for...in iterates over property names;
- iteruje v ramci objektu a vraci jednotlive elementy objektu;
- v ramci "array" je i = indexu elementu; v ramci "object" je rovno "key" properte;

    for(i in cats) {
        console.log(i);
    }
        // 0
        // 1
        // 2
        // 3
        // 4

/// FOR...OF
    for (variable of object)
      statement
- for...of iterates over property values;
- v ramci "array" je i = hodnote elementu; v ramci "object" je rovno hodnote prirazene ke "key";

    for(i of cats) {
        console.log(i)
    }
        // Bill
        // Jeff
        // Pete
        // Biggles
        // Jasmin


/// WHILE
    initializer
    while (condition) {
      // code to run

      final-expression
    }

- works in a very similar way to the "for loop", except that the initializer variable is set before the loop;
- also the final-expression is included inside the loop after the code to run;
- the final-expression is run after the code inside the loop has run (an iteration has been completed),
    which will only happen if the condition is still true;

    let i = 0;

    while (i < cats.length) {
      if (i === cats.length - 1) {
        info += 'and ' + cats[i] + '.';
      } else {
        info += cats[i] + ', ';
      }

      i++;
    }

/// DO...WHILE
    initializer
    do {
      // code to run

      final-expression
    } while (condition)

- differentiator here is that the condition comes after everything else;
- the code inside the curly braces is always (after keyword "do") run once before the check is made to see if it
    should be executed again (in "while" and "for", the check comes first, so the code might never be executed);

    let i = 0;

    do {
      if (i === cats.length - 1) {
        info += 'and ' + cats[i] + '.';
      } else {
        info += cats[i] + ', ';
      }

      i++;
    } while (i < cats.length);


|| FUNCTIONS

    function myFunction() {
      alert('hello');
    }
    myFunction();

    function square(number) {
      return number * number;
    }
    square(2);

- in JS there are "built-in functions", as well as the "built-in objects" and their corresponding "methods";
- creating a function:  "function definition" / "function declaration" / "function statement";
- "calling of function" / "function invoking";
- function declaration is always hoisted, so you can call function above function definition and it will work fine;
- for function naming conventions, you should follow the same rules as variable naming conventions;

/// ANONYMOUS FUNCTIONS
- generally use an anonymous function along with an event handler:

    const myButton = document.querySelector('button');
    myButton.onclick = function() {
      alert('hello');
    }

- also assign an anonymous function to be the value of a variable:

    const myGreeting = function() {
      alert('hello');
    }
    myGreeting();

- this form of creating a function is also known as "function expression";
- you can also assign the function to be the value of multiple variables;
    let anotherGreeting = myGreeting;
    - this function could now be invoked using either of:
        myGreeting();
        anotherGreeting();

- mainly use of anonymous functions is to just run a load of code in response to an event firing — like a button
    being clicked — using an event handler;

/// FUNCTION EXPRESSIONS
- unlike function declaration, function expressions are not hoisted;
- however, a name can be provided with a function expression;
    - providing a name allows the function to refer to itself, and also makes it easier to identify the function
        in a debugger's stack traces;
- function expressions are convenient when passing a function as an argument to another function;

/// FUNCTION PARAMETERS
- some functions require parameters to be specified when you are invoking them;
- parameters are sometimes called "arguments", "properties", or even "attributes";
-  when you need to specify multiple parameters, they are separated by commas;

/// FUNCTION SCOPE
- when you create a function, the variables and other things defined inside the function are inside their own
    separate scope, meaning that they are locked away in their own separate compartments, unreachable
    from code outside the functions;
- the top level outside all your functions is called the GLOBAL SCOPE; Values defined in the global scope are accessible
    from everywhere in the code;

** Functions inside functions
- this is often used as a way to keep code tidy — if you have a big complex function, it is easier to understand
    if you break it down into several sub-functions;

** FUNCTION INVOKE WITHIN A CODE
- je zde rozdil, pokud fci volame primo s uzitim jejiho jmena (at uz z fcn expression nebo fcn declaration) nebo pri
    vyuziti event handleru nebo jeji volani umistujeme do promenne;
- rozdil mezi:
    btn.onclick = displayMessage;
    a
    btn.onclick = displayMessage();
    je v tom, ze v 1. pripade, se fce spusti az po te, co je zaznamenan event (v tomto pripade "onclick" na promenne btn),
        kdezto v 2. pripade, se fce spusti okamzite, jakmile k ni engine v kodu dojde, neceka se na event;

- btn.onclick = displayMessage(); - parentheses in this context are sometimes called the "function invocation operator";
    - you only use them when you want to run the function immediately in the current scope;

    closeBtn.onclick = function() {
        panel.parentNode.removeChild(panel);
    }
        - v tomto pripade se kod take hned nespusti, ptze kod uvnitr anonymni fce je pouze v ramci fcn scope;

/// RETURN VALUES
- some functions don't return any value (more precisely said they return void or undefined values);
- generally, a return value is used where the function is an intermediate step in a calculation of some kind
    (it can return the result so it can be stored in a variable);
- to return a value from a custom function, you need to use the "return" keyword; the return value appears at the point
    the function was called;

    function random(number) {
      return Math.floor(Math.random() * number);
    }
    ctx.arc(random(WIDTH), random(HEIGHT), random(50), 0, 2 * Math.PI);

    - the function calls (random(WIDTH), random(HEIGHT)) on the line are run first, and their return values substituted
        for the function calls, before the line itself is then executed;

** RETURN statement
- the "return" statement ends function execution and specifies a value to be returned to the function caller;
    return [expression];
- the expression whose value is to be returned; if omitted, "undefined" is returned instead;
- the return statement is affected by "automatic semicolon insertion" (ASI); no line terminator is allowed
    between the return keyword and the expression;
        return
        a + b;
        is transformed by ASI into:
        return;
        a + b;
    - to avoid this problem (to prevent ASI), you could use parentheses:
        return (
          a + b
        );


|| EVENTS
= are actions or occurrences that happen in the system, which the system tells you about so you can respond to them
    in some way if desired;
- EVENT HANDLERS: each available event has an EVENT HANDLER, which is a block of code (usually a JS function
    that you as a programmer create) that runs when the event fires;
- when such a block of code is defined to run in response to an event, we say we are "registering an event handler";
- EVENT LISTENERS listen out for the event happening (and the handler is the code that is run in response to it happening);
- Web events are not part of the core JS language — they are defined as part of the APIs built into the browser;

// EVENT HANDLER PROPERTIES
= are the properties that exist to contain event handler code;
- e.g. "onclick" is a property like any other available on the button element but it is a special type — when you set it
    to be equal to some code, that code is run when the event fires on the button;

    <button>Change color</button>

    const btn = document.querySelector('button');

    function random(number) {
        return Math.floor(Math.random() * (number+1));
    }

    btn.onclick = function() {
        const rndCol = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
        document.body.style.backgroundColor = rndCol;
    }

- other examples:
btn.onfocus, btn.onblur, btn.ondblclick, window.onkeydown, window.onkeyup, btn.onmouseover and btn.onmouseout
- some events are general and available nearly anywhere (e.g. an onclick handler can be registered on nearly any element),
    whereas some are more specific and only useful in certain situations;

!!!!! DO NOT USE INLINE EVENT HANDLERS !!!!!
such as:
    <button onclick="bgChange()">Press me</button>

    function bgChange() {
        const rndCol = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
        document.body.style.backgroundColor = rndCol;
    }
- the earliest method of registering event handlers found on the Web involved event handler HTML attributes
    (or inline event handlers) like the one shown above — the attribute value is literally the JS code you want to run
    when the event occurs;
    - the above example invokes a function defined inside a <script> element on the same page, but you could also insert
    JS directly inside the attribute, for example:
        <button onclick="alert('Hello, this is my old-fashioned event handler!');">Press me</button>

- keeping your JS separate is best practice; if it is in a separate file you can apply it to multiple HTML documents;
- separating your programming logic from your content also makes your site more friendly to search engines;
- even in a single file, inline event handlers are not a good idea - one button is OK, but what if you had 100 buttons?
    - you'd have to add 100 attributes to the file; it would quickly turn into a maintenance nightmare;
- with JS, you could easily add an event handler function to all the buttons on the page no matter how many
    there were, using something like this:

        const buttons = document.querySelectorAll('button');

        for (let i = 0; i < buttons.length; i++) {
            buttons[i].onclick = bgChange;
        }

        or:

        buttons.forEach(function(button) {
            button.onclick = bgChange;
        });

// ADDING AND REMOVING EVENT HANDLERS
- modern mechanism for adding event handlers is the addEventListener() method:

    const btn = document.querySelector('button');

    function bgChange() {
        const rndCol = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
        document.body.style.backgroundColor = rndCol;
    }

    btn.addEventListener('click', bgChange);

- inside the addEventListener() function, we specify two parameters:
    - the name of the event we want to register this handler for
    - the code that comprises the handler function we want to run in response to it.
- Note: It is perfectly appropriate to put all the code inside the addEventListener() function, in an anonymous function,
    like this:

    btn.addEventListener('click', function() {
        var rndCol = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
        document.body.style.backgroundColor = rndCol;
    });

- advantages of this mechanism are:
    - there is a counterpart function, removeEventListener();
        - ability to remove event handlers allows you to have the same element performing different actions in different
        circumstances -> for larger, more complex programs, it can improve efficiency;
    - it allows you to register multiple handlers for the same listener;
        - this does not work as second line overwrites the value of onclick set by the first line:
            myElement.onclick = functionA;
            myElement.onclick = functionB;
        - but this will work and both functions would now run when the element is selected:
            myElement.addEventListener('click', functionA);
            myElement.addEventListener('click', functionB);
            - so you can add multiple listeners of the same type (with different functions specified in the second argument)
                to elements if required;

// EVENT OBJECTS
- sometimes inside an event handler function, you'll see a parameter specified with a name such as "event", "evt" or "e";
- this is called the "event object", and it is automatically passed to event handlers to provide extra features and information;

    function bgChange(e) {
      const rndCol = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
      e.target.style.backgroundColor = rndCol;
      console.log(e);
    }

    btn.addEventListener('click', bgChange);

- e.target = target property of the event object is always a reference to the element the event occurred upon
    (so, in the example above, we are setting a random background color on the button, not the page);
    - e.target is incredibly useful when you want to set the same event handler on multiple elements and do something
        to all of them when an event occurs on them;

        const divs = document.querySelectorAll('div');

        for (let i = 0; i < divs.length; i++) {
          divs[i].onclick = function(e) {
            e.target.style.backgroundColor = bgChange();
          }
        }

// PREVENTING DEFAULT BEHAVIOR
- sometimes, you'll come across a situation where you want to prevent an event from doing what it does by default;
- the most common example is that of a web form -  trouble comes when the user has not submitted the data correctly
- we call the preventDefault() function on the event object to prevent default behavior:

    <form>
      <div>
        <label for="fname">First name: </label>
        <input id="fname" type="text">
      </div>
      <div>
        <label for="lname">Last name: </label>
        <input id="lname" type="text">
      </div>
      <div>
         <input id="submit" type="submit">
      </div>
    </form>
    <p></p>

    const form = document.querySelector('form');
    const fname = document.getElementById('fname');
    const lname = document.getElementById('lname');
    const para = document.querySelector('p');

    form.onsubmit = function(e) {
      if (fname.value === '' || lname.value === '') {
        e.preventDefault(); // stops the form submission
        para.textContent = 'You need to fill in both names!';
      }
    }

// EVENT BUBBLING AND CAPTURE
- are two mechanisms that describe what happens when two handlers of the same event type are activated on one element
- when an event is fired on an element that has parent elements, modern browsers run three different phases:
    - the capturing phase:
        - browser checks to see if the element's outer-most ancestor (<html>) has an "onclick" event handler registered
            on it for the capturing phase, and runs it if so;
        - then it moves on to the next element inside <html> and does the same thing, then the next one, and so on
            until it reaches the direct parent of the element that was actually selected;
    - the target phase:
        - browser checks to see if the target property has an event handler for the "click" event registered on it,
            and runs it if so;
        - then, if "bubbles" is true, it propagates the event to the direct parent of the selected element, then the next one,
            and so on until it reaches the <html> element;
            - otherwise, if "bubbles" is false, it doesn’t propagate the event to any ancestors of the target;
        - the "bubbles" read-only property of the "Event" interface indicates whether the event bubbles up through the DOM or not;
    - the bubbling phase:
        - in the bubbling phase, the exact opposite of the capturing phase occurs:
            - browser checks to see if the direct parent of the element selected has an "onclick" event handler
                registered on it for the bubbling phase, and runs it if so;
            - then it moves on to the next immediate ancestor element and does the same thing, then the next one,
                and so on until it reaches the <html> element;
- event listeners registered for the <html> element aren’t at the top of hierarchy; for example, event listeners
    registered for the "window" and "document" objects are higher in the hierarchy;

- this is a very annoying behavior, but there is a way to fix it - standard Event object has a function available on it
    called stopPropagation() which, when invoked on a handler's event object, makes it so that first handler is run
    but the event doesn't bubble any further up the chain, so no more handlers will be run;

- the example code is shown in exercises directory in a file fixing-bubbling-event.html;

- the W3C decided to try to standardize the behavior and reach a consensus, they ended up with this system
    that included both - capturing and bubbling - which is the one modern browsers implemented;
- by default all event handlers are registered in the bubbling phase, and this makes more sense most of the time
    (it can be changed to the capturing phase instead but must be done manually);

// EVENT DELEGATION
- Bubbling also allows us to take advantage of event delegation — this concept relies on the fact that if you want
    some code to run when you select any one of a large number of child elements, you can set the event listener
    on their parent and have events that happen on them bubble up to their parent rather than having to set the event
    listener on every child individually;
- good example is a series of list items -  if you want each one to pop up a message when selected, you can set
    the click event listener on the parent <ul>, and events will bubble from the list items to the <ul>;


|| JS OBJECTS

- object is a collection of related data and/or functionality which usually consists of several variables and functions
    — which are called "properties" and "methods" when they are inside objects;
    - object's properties: data items - string, number, array;
    - object's methods: functions;

SYNTAX
-> object like this is referred to as an "object literal" (we've literally written out the object contents as we've
    come to create it):
const objectName = {
  member1Name: member1Value,
  member2Name: member2Value,
  member3Name: member3Value
};

const person = {
  name: ['Bob', 'Smith'],
  age: 32,
  gender: 'male',
  interests: ['music', 'skiing'],
  bio: function() {
    alert(this.name[0] + ' ' + this.name[1] + ' is ' + this.age + ' years old. He likes ' + this.interests[0] + ' and ' + this.interests[1] + '.');
  },
  greeting: function() {
    alert('Hi! I\'m ' + this.name[0] + '.');
  }
};

// DOT NOTATION
- you access the object's properties and methods using "dot notation";
- the object name acts as the "namespace" — it must be entered first to access anything encapsulated inside the object;
- next you write a dot, then the item you want to access — this can be the name of a simple property,
    an item of an array property, or a call to one of the object's methods, for example:
        person.age
        person.interests[1]
        person.bio()

** SUB-NAMESPACES
- change name: ['Bob', 'Smith'], to:
name : {
  first: 'Bob',
  last: 'Smith'
}
-> this is a sub-namespace (object within object): person.name.first;

// BRACKET NOTATION
- another way to access object properties;
    person['age']
    person['name']['first']
-> to select an item, you are using the name associated with each member's value;
- due to it objects are sometimes called "associative arrays" (they map strings to values in the same way that
    arrays map numbers to values);

// SETTING OBJECT MEMBERS
- you can set (update) the value of object members OR create completely new members;

    person.age = 45;
    person['name']['last'] = 'Cratchit';

    person['eyes'] = 'hazel';
    person.farewell = function() { alert("Bye everybody!"); }

- one useful aspect of bracket notation is that it can be used to set not only member values dynamically,
    but member names too:
        let myDataName = 'height';
        let myDataValue = '1.75m';
        person[myDataName] = myDataValue;
        -> opravdu se vytvori properta s key "height" // person.height -> 1.75m
- adding a property to an object using the method above isn't possible with dot notation, which can only accept
    a literal member name, not a variable value pointing to a name;
        let myDataName = 'height';
        let myDataValue = '1.75m';
        person.myDataName = myDataValue;
        -> vytvori se properta s key "myDataName", ne height // person.myDataName -> 1.75m


// KEYWORD "THIS"
- "this" keyword refers to the current object the code is being written inside;

// OBJECTS ALL ALONG
- every time we've been working through an example that uses a built-in browser API or JS object, we've been using objects;
- when you use string methods (like split) you are using a method available on an instance of the "String" class -
    every time you create a string in your code, that string is automatically created as an instance of String,
    and therefore has several common methods and properties available on it;
- when you access the document object model (document.querySelector('video')) you were using methods available
    on an instance of the "Document" class - for each webpage loaded, an instance of Document is created,
        called "document", which represents the entire page's structure, content, and other features such as its URL;
- the same is true of pretty much any other built-in object or API you've been using — Array, Math, and so on;
(NOTE: built-in objects and APIs don't always create object instances automatically);


// OBJECT-ORIENTED JS FOR BEGINNERS (OOJS)
- Object data (and often, functions too) can be stored neatly (the official word is "encapsulated")
    inside an object package (which can be given a specific name to refer to, which is sometimes called a "namespace"),
    making it easy to structure and access;
- "abstraction" = creating a simple model of a more complex thing, which represents its most important aspects
    in a way that is easy to work with for our program's purposes;
    - keyword Class
- from our "class", we can create "object instances" - objects that contain the data and functionality defined in the class;
- when an object instance is created from a class, the class's "constructor function" is run to create it;
    - process of creating an object instance from a class is called "instantiation" — the object instance
        is instantiated from the class;
- In OOP, we can create new classes based on other classes — these new child classes (also known as "subclasses")
    can be made to inherit the data and code features of their parent class, so you can reuse functionality common
    to all the object types rather than having to duplicate it;
    - where functionality differs between classes, you can define specialized features directly on them as needed;
- polymorphism = the ability of multiple object types to implement the same functionality;

** CONSTRUCTORS AND OBJECT INSTANCES
example code in the file "object-constructor.html" in "exercises" directory;

- JS uses special functions called "constructor functions" to define and initialize objects and their features;
- constructor function is JS's version of a class;

    function Person(name) {
      this.name = name;
      this.greeting = function() {
        alert('Hi! I\'m ' + this.name + '.');
      };
    }

- it doesn't return anything or explicitly create an object — it basically just defines properties and methods;
- name convention is to start a constructor function name with a capital letter;

- creation of object instances:

    let person1 = new Person('Bob');
    let person2 = new Person('Sarah');

- when we are calling our constructor function, we are defining greeting() every time, which isn't ideal;
    - to avoid this, we can define functions on the prototype instead (explained below);

** OTHER WAYS TO CREATE OBJECT INSTANCES
- different ways to create an object instance:
    - declaring an object literal;
    - using a constructor function;
    - the Object() constructor;
    - create() method;

-- OBJECT() CONSTRUCTOR
- generic objects have a constructor, which generates an empty object;
    let person1 = new Object();
- you can then add properties and methods to this object using dot or bracket notation;
- you can also pass an object literal to the Object() constructor as a parameter, to prefill it with properties/methods:
    let person1 = new Object({
      name: 'Chris',
      age: 38,
      greeting: function() {
        alert('Hi! I\'m ' + this.name + '.');
      }
    });

-- CREATE() METHOD
- to create object instances without first creating constructors;
- it is JS built-in method;
- with it, you can create a new object, using an existing object as the prototype of the newly created object;

    let person2 = Object.create(person1);


// OBJECT PROTOTYPES

- Prototypes are the mechanism by which JS objects inherit features from one another;
- "prototype" property;
- JS is often described as a "prototype-based language" - to provide inheritance, object can have a "prototype object",
    which acts as a template object that it inherits methods and properties from;
- object's prototype object may also have a prototype object, which it inherits methods and properties from, and so on -
    this is often referred to as a "prototype chain", and explains why different objects have properties and methods
        defined on other objects available to them;
- in JS, a link is made between the object instance and its prototype (its __proto__ property, which is derived
    from the "prototype" property on the constructor), and the properties and methods are found/accessed
        BY WALKING UP THE CHAIN OF PROTOTYPES (so the methods and properties ARE NOT COPIED from one object to another
            in the prototype chain);

!!!!! Note:
It's important to understand that there is a distinction between:
    - "an object's prototype" (available via Object.getPrototypeOf(obj), or via the deprecated __proto__ property);
    - and the "prototype property" on constructor functions";
EXAMPLE:
- the constructor function Foobar() has its own "prototype", which can be found by calling: Object.getPrototypeOf(Foobar);
- however this differs from its "prototype property" = Foobar.prototype - which is the blueprint for instances
    of this constructor function;
- if we were to create a new instance: let fooInstance = new Foobar()
    "fooInstance" would take its "prototype" from its constructor function's "prototype property" thus:
        Object.getPrototypeOf(fooInstance) === Foobar.prototype;
!!!!!

Note: The prototype chain is traversed only while retrieving properties;
- if properties are set or deleted directly on the object, the prototype chain is not traversed;

Note: Before ECMAScript 2015, there wasn't officially a way to access an object's prototype directly — the "links"
    between the items in the chain are defined in an internal property, referred to as [[prototype]] in the specification
    for the JavaScript language (see ECMAScript);
    - since ECMAScript 2015, you can access an object's prototype object indirectly via Object.getPrototypeOf(obj);

** The prototype property: Where inherited members are defined
- the inherited members are the ones defined on the "prototype property" (you could call it a sub-namespace);
    so the ones that begin with "Object.prototype. "(Object.prototype.member), and not the ones that begin with
        just "Object. "(Object.member);
- prototype property's value is an object, which is basically a bucket for storing properties and methods
    that we want to be inherited by objects further down the prototype chain;
(e.g. Object.prototype.toString(), Object.prototype.valueOf(),...)
- members not defined inside the prototype bucket are not inherited by object instances and are available
    just on the Object() constructor itself (e.g. Object.is(), Object.keys(), ...) = these are also known as
    "static properties/methods";

Note: because of the way JS works, with the prototype chain, etc., the sharing of functionality between
    objects is often called "delegation" - specialized objects delegate functionality to a generic object type;

** Object.create() method creates a new object from a specified prototype object;
    let person2 = Object.create(person1);
    -> person2 is being created using person1 as a prototype object;

** THE CONSTRUCTOR PROPERTY
- every constructor function has a prototype property whose value is an object containing a "constructor" property;
- this "constructor" property points to the original constructor function;
- properties defined on the Person.prototype property become available to all the instance objects created using
    the Person() constructor;
- clever trick is that you can put parentheses onto the end of the "constructor" property (with/without parameters)
    to create another object instance from that constructor;
- "constructor" is a function after all, so can be invoked using parentheses, just need to include the "new" keyword
    to specify that you want to use the function as a "constructor";

        let person3 = new person1.constructor('Karen', 'Stephenson', 26, 'female', ['playing drums', 'mountain climbing']);

- it can be really useful when you want to create a new instance and don't have a reference to the original constructor
    easily available for some reason;
- "constructor" property has other uses: if you have an object instance and you want to return the name of the constructor
    it is an instance of: instanceName.constructor.name;
        person1.constructor.name
Note: The value of constructor.name can change (due to prototypical inheritance, binding, preprocessors, transpilers, etc.)
    - therefore, for more complex examples, you'll want to use the "instanceof" operator instead;

** MODIFYING PROTOTYPES
example of functional code in the file "object-constructor.html" in "exercises" directory;

    Person.prototype.farewell = function() {
      alert(this.name.first + ' has left the building. Bye for now!');
    };

- the whole inheritance chain has updated dynamically, automatically making this new method available on all object
    instances derived from the constructor (Person);

- deleting properties defined on the constructor's prototype using the "delete" operator removes the respective
    properties from all other class instances too:

        delete Person.prototype.farewell

- or you could use Object.defineProperty() instead;

- you will rarely see properties defined on the "prototype property", because they are not very flexible when defined like this:
    Person.prototype.fullName = 'Bob Smith';
-> so you might define constant properties on the prototype (i.e. ones that never need to change),
    but generally it works better to define properties inside the constructor;

-fairly common pattern for more object definitions is to define the properties inside the constructor,
    and the methods on the prototype:

        function Test(a, b, c, d) {
          // property definitions
        }

        // First method definition

        Test.prototype.x = function() { ... };

        // Second method definition

        Test.prototype.y = function() { ... };


// INHERITANCE IN JS
example of functional code in the file "class-inheritance.html" in "exercises" directory;

- if we want to inherit from another constructor function we need to use "call" function:

    function Teacher(first, last, age, gender, interests, subject) {
      Person.call(this, first, last, age, gender, interests);

      this.subject = subject;
    }

- the call() function basically allows you to call a function defined somewhere else, but in the current context;
- the first parameter specifies the value of "this" that you want to use when running the function,
    and the other parameters are those that should be passed to the function when it is invoked;
-  last line inside the Teacher constructor defines the new "subject" property that teachers are going to have,
    which generic people (Person constructor) don't have;

NOTE: Inheriting from a constructor with no parameters:

    function Brick() {
      this.width = 10;
      this.height = 20;
    }

    function BlueGlassBrick() {
      Brick.call(this);

      this.opacity = 0.5;
      this.color = 'blue';
    }

** Setting Teacher()'s prototype and constructor reference
- now we have new constructor and it has a "prototype property", which by default just contains an object with a
    reference to the constructor function itself but it does not contain the methods of the Person constructor's
    prototype property;
- we can see this through:
    Object.getOwnPropertyNames(Teacher.prototype)
    Object.getOwnPropertyNames(Person.prototype)
- we need to get Teacher() to inherit the methods defined on Person()'s prototype. So how do we do that?

    Teacher.prototype = Object.create(Person.prototype);

-> we just set Teacher.prototype to reference an object that inherits its properties from Person.prototype so
    Teacher.prototype's constructor property is now equal to Person() - we can verify it through
    Teacher.prototype.constructor;
- to finish it to be shown as Teacher() and also inherit from Person() we need to add the following:

    Object.defineProperty(Teacher.prototype, 'constructor', {
        value: Teacher,
        enumerable: false, // so that it does not appear in 'for in' loop
        writable: true });

** ECMAScript 2015 CLASSES
- ECMAScript 2015 introduces class syntax to JS as a way to write reusable classes using easier, cleaner syntax;
- is supported in all modern browsers (only old IE doesn't support this);
- the "class statement" indicates that we are creating a new class; inside this block, we define all the features of the class:

    class Person {
      constructor(first, last, age, gender, interests) {
        this.name = {
          first,
          last
        };
        this.age = age;
        this.gender = gender;
        this.interests = interests;
      }

      greeting() {
        console.log(`Hi! I'm ${this.name.first}`);
      };

      farewell() {
        console.log(`${this.name.first} has left the building. Bye for now!`);
      };
    }

- the constructor() method defines the constructor function;
- class methods you want associated with the class are defined inside it, after the constructor();
- under the hood, your classes are being converted into Prototypal Inheritance models;

-- INHERITANCE WITH CLASS SYNTAX
example of functional code in the file "class-inherit-ES6.html" in "exercises" directory;
- creating a subclass / subclassing;
- To create a subclass we use the "extends" keyword:

    class Teacher extends Person {
      constructor(subject, grade) {
        this.subject = subject;
        this.grade = grade;
      }
    }

- unlike old-school constructor functions where the "new" operator does the initialization of "this" to a
    newly-allocated object, this isn't automatically initialized for a "class" defined by the "extends" keyword,
    i.e the sub-classes;
- for sub-classes, the "this" initialization to a newly allocated object is always dependant on the parent class
    constructor, i.e the constructor function of the class from which you're extending (for Teacher, the "this"
    initialization is done by the Person constructor);
-> to call the parent constructor we have to use the super() operator, like so:

    class Teacher extends Person {
      constructor(subject, grade) {
        // Now 'this' is initialized by calling the parent constructor:
        super(first, last, age, gender, interests);

        // subject and grade are specific to Teacher:
        this.subject = subject;
        this.grade = grade;
      }
    }

- the super() operator is actually the parent class constructor, passing it the necessary arguments of the Parent class
    constructor will also initialize the parent class properties in our sub-class;

** GETTERS AND SETTERS
- can handle situations when we want to change the values of an attribute in the classes we create or we don't know
    what the final value of an attribute will be;
- getters and setters work in pairs - a getter returns the current value of the variable
    and its corresponding setter changes the value of the variable to the one it defines;
- modified Teacher class looks like this:

    class Teacher extends Person {
      constructor(first, last, age, gender, interests, subject, grade) {
        super(first, last, age, gender, interests);

        this._subject = subject;
        this.grade = grade;
      }

      get subject() {
        return this._subject;
      }

      set subject(newSubject) {
        this._subject = newSubject;
      }
    }

- in our class above we have a getter and setter for the subject property;
    - we use _ to create a separate value in which to store our name property - without using this convention,
     we would get errors every time we called get or set;


// JSON
= JavaScript Object Notation
= a standard text-based format for representing structured data based on JS object syntax;
- is commonly used for transmitting data in web applications (e.g., sending some data from the server to the client,
    so it can be displayed on a web page, or vice versa);
- can be used independently from JS, and many programming environments feature the ability to read (parse)
    and generate JSON;
- JSON exists as a string — useful when you want to transmit data across a network;
- it needs to be converted to a native JS object when you want to access the data —  JS provides a global "JSON" object
    that has methods available for converting between the two;

Note: Converting a string to a native object is called "deserialization", while converting a native object to
    a string so it can be transmitted across the network is called serialization;

- JSON string can be stored in its own file, which is basically just a text file with an extension of .json;
- JSON is purely a string with a specified data format — it contains only properties, no methods;
- SON requires double quotes to be used around strings and property names. Single quotes are not valid other than
    surrounding the entire JSON string;
- you can validate JSON using an application like https://jsonlint.com/;
- JSON can actually take the form of any data type that is valid for inclusion inside JSON, not just arrays or objects
    (e.g. a single string or number);

** OBTAIN JSON
example of functional code in the file "JSON-playing.html" in "exercises" directory;

- to obtain the JSON, we use an API called "XMLHttpRequest" (often called XHR) - this is a very useful JS object
    that allows us to make network requests to retrieve resources from a server via JS (e.g. images, text, JSON,
    even HTML snippets);

- we store the URL of the JSON we want to retrieve in a variable:
    let requestURL = 'https://mdn.github.io/learning-area/javascript/oojs/json/superheroes.json';
- to create a request, we need to create a new request object instance from the XMLHttpRequest constructor:
    let request = new XMLHttpRequest();
- we need to open the request using the open() method - takes at least two parameters:
    request.open('GET', requestURL);
- we are setting the "responseType" to JSON, so that XHR knows that the server will be returning JSON,
    and that this should be converted behind the scenes into a JS object;
     - then we send the request with the send() method:
        request.responseType = 'json';
        request.send();
- last bit of this section involves waiting for the response to return from the server, then dealing with it
    request.onload = function() {
      const superHeroes = request.response;
      populateHeader(superHeroes);
      showHeroes(superHeroes);
    }
-> we are storing the response to our request (available in the response property) in a variable called superHeroes;
-> this variable now contains the JS object based on the JSON;
- we have wrapped the code in an event handler that runs when the load event fires on the request object — this is
    because the load event fires when the response has successfully returned;
    - doing it this way guarantees that request.response will definitely be available when we come to try to do something with it;

- at the end there are defined functions we need to fill data to show - see the "JSON-playing.html" file;


** CONVERTING BETWEEN OBJECTS AND TEXT
- sometimes we receive a raw JSON string, and we need to convert it to an object ourselves;
- when we want to send a JS object across the network, we need to convert it to JSON (a string) before sending;
- these two problems are so common in web development that a built-in JSON object is available in browsers,
    which contains the following two methods:
        - parse(): accepts a JSON string as a parameter, and returns the corresponding JS object;
        - stringify(): accepts an object as a parameter, and returns the equivalent JSON string;
- now we set the XHR to return the raw JSON text, then used parse() to convert it to an actual JS object;
-t he key snippet of code is here:
    request.open('GET', requestURL);
    request.responseType = 'text'; // now we're getting a string!
    request.send();

    request.onload = function() {
      const superHeroesText = request.response; // get the string from the response
      const superHeroes = JSON.parse(superHeroesText); // convert it to an object
      populateHeader(superHeroes);
      showHeroes(superHeroes);
    }

// OBJECT BUILDING PRACTICE
example of functional code in the file "bounce-balls.html" in "exercises" directory;






































*/

