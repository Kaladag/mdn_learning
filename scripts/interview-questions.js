/* JAVASCRIPT INTERVIEW QUESTIONS

|| CODING ISSUES

https://www.interviewbit.com/javascript-interview-questions/#coding-problems

************************************************************************************************************************

|| What is JavaScript?

JavaScript is a client-side and server-side scripting language;
client-side = code that is run on the user's computer - when a web page is viewed, the page's client-side code
    is downloaded, then run and displayed by the browser;
server-side = is run on the server, then its results are downloaded and displayed in the browser, popular Node.js;

************************************************************************************************************************

|| What are the advantages of JavaScript?

Lightweight: It is easy to implement. It has small memory footprints.
Interpreted: It is an interpreted language. Instructions are executed directly.
Object-oriented: It is an object-oriented language.
First-class functions: In JavaScript, a function can be used as a value.
Scripting Language: It’s a language in which instructions are written for a run-time environment.

************************************************************************************************************************

|| Enumerate the differences between Java and JavaScript?

Java is a full-fledged object-oriented programming (OOP) like C++ or C#, whereas
    JavaScript is a scripting language;

Oba výše zmíněné jazyky pracují na naprosto rozdílné filosofii, byly založeny v jiné době, ani syntaxí na tom nejsou stejně.

JS:
Asynchronní scriptovací jazyk pro tvorbu interaktivních webových aplikací na Front-Endu.
Vytvořen byl společností Netscape, následně byl předán ke standardizaci ECMA (European Computer Manufacturer’s Association).
Z důvodu problémů s názevem dostala standardizovaná verze podivný název ECMAScript.
Uživateli umožní reagovat se stránkou, aniž by posílal dotaz serveru, nebo komunikovat se serverem,
    aby uživatel nebyl obtěžován překreslováním stránky jako celku. Je tedy schopen měnit data pouze u klienta.
Všechno je bráno jako objekt, neexistují klasické třídy, využívá se tzv. prototypů.
Zpracování kódu probíhá v jednom vlákně - jednovláknový (single threaded) =  zpracovává pouze jeden příkaz v daný okamžik
    a je náchylný k zamrznutí.

JAVA:
Využití Javy na webech není problém, u důležitých projektů je často volena na straně serveru namísto PHP,
    kodér by ale její využití hledal marně.
Java byla vytvořena společností Sun Microsystems (nyní Oracle).
V dnešní době je často známá především díky programování na Android, kde se používá jako primární jazyk pro vývoj.
Jinak je to ale jeden z nejpoužívanějších a nejoblíbenějších programovacích jazyků na světě.
V Javě se píší nativní programy, u nichž je vyžadována bezpečnost spolu s rychlým vývojem. Zároveň není potřeba psát
    a kompilovat program pro různé systémy zvlášť, stačí jeden kód, který se díky interpretovanému přístupu chová
    na všech platformách stejně.
Zakládá si na objektovém přístupu, nezávislosti na architektuře a v základu synchronním zpracováním funkce.
Vícevláknové zpracování kódu.

************************************************************************************************************************

|| What are JavaScript Data Types?

Primitive values:
Boolean
Null
Undefined
(ECMAScript has two built-in numeric types): Number and BigInt
String
Symbol

Objects (Object, Array, and Function)

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures

************************************************************************************************************************

|| What is a Truthy or Falsy value?

https://while.dev/articles/explaining-truthy-falsy-null-0-and-undefined-in-typescript/

Every possible variable value of any type will be implicitly converted to a boolean true or false value if needed.
The need to do so is usually caused by an if-statement, having a non-boolean condition value.
This implicit conversion from non-boolean values to boolean is based on some rules.
The term truthy describes all values that will be converted to true.
The term falsy describes all values that will be converted to false.

Truthy values
true ⸺ The boolean value true stays true.
{} new Date() [] ⸺ Non primitive values get converted to true (including 'empty' ones).
42 -42 3.14 -3.14 ⸺ All non-zero numbers get converted to true.
Infinity -Infinity ⸺ Infinity values get converted to true.
"dog" 'cat'' ⸺ Non-empty strings get converted to true.

Falsy values
false ⸺ The boolean value false stays false.
null ⸺ Null gets converted to false.
undefined ⸺ Undefined gets converted to false.
0 ⸺ The number 0 gets converted to false.
NaN ⸺ The value for 'Not a Number' gets converted to false.
'' "" ⸺ Empty strings get converted to false.

Ugly exception ☹
"document.all" gets converted to false, even if it's a collection filled with items.
This clearly violates the rule "Non primitive values get converted to true (including 'empty' ones)".

************************************************************************************************************************

|| What is the difference between null, undefined and 0?

0 ⸺ It's just the number zero.
undefined ⸺ Undefined means that something is not defined - The value undefined means something like:
    The accessed property doesn't even exist.
null ⸺ Null means that something was defined but is not set - The value null means something like:
    The accessed property exists but there is no value set.

************************************************************************************************************************

|| What do you mean by 'null' in Javascript?

The value null represents the intentional (záměrnou) absence of any object value.
It is one of JavaScript's primitive values and is treated as falsy for boolean operations.

************************************************************************************************************************

|| What is an undefined value in JavaScript?

The global "undefined" property represents the primitive value "undefined".
It is one of JavaScript's primitive types.
A variable that has not been assigned a value is of type undefined.
A method or statement also returns undefined if the variable that is being evaluated does not have an assigned value.
A function returns undefined if a value was not returned.

************************************************************************************************************************

|| What is NaN? What is its type?

The NaN property represents a value that is “not a number”.
This special value results from an operation that could not be performed either because one of the operands
    was non-numeric (e.g., "abc" / 4), or because the result of the operation is non-numeric.
Although NaN means “not a number”, its type is 'Number':
    console.log(typeof NaN === "number");  // logs "true"
Additionally, NaN compared to anything – even itself! – is false:
    console.log(NaN === NaN);  // logs "false"

************************************************************************************************************************

|| What is difference between == and === operators?

Both are JS comparison operators

===
- is "strict equality operator" which returns true when the two operands have the same value without conversion.

==
- is "double equals operator" which tries to be smart in a very unintuitive way.
If you use it to compare two values of a different type, it will try to convert them to a common type.
The common type is usually boolean, therefore it converts both sides to a boolean, according to above conversion rules
    (Truthy or Falsy). They only get compared after the conversion!
(Other words: == or != test whether the values are the same but not whether the values' data types are the same
    - they tend to result in errors so not recommended for use)

Some more examples:

undefined == null ⟶ false == false ⟶ true
undefined === null ⟶ false

undefined == 0 ⟶ false == false ⟶ true
undefined === 0 ⟶ false

undefined == null ⟶ false == false ⟶ true
undefined === null ⟶ false

'' == 0 ⟶ false == false ⟶ true
'' === 0 ⟶ false

************************************************************************************************************************

|| What is the use of isNaN() function?

The isNaN() function determines whether a value is NaN or not.
Return value - true if the given value is NaN; otherwise, false.
Unlike all other possible values in JavaScript, it is not possible to use the equality operators (== and ===)
    to compare a value against NaN to determine whether the value is NaN or not, because both NaN == NaN and NaN === NaN
    evaluate to false.
The isNaN() function provides a convenient equality check against NaN.
*/
function milliseconds(x) {
    if (isNaN(x)) {
        return 'Not a Number!';
    }
    return x * 1000;
}

console.log(milliseconds('100F'));
// expected output: "Not a Number!"

console.log(milliseconds('0.0314E+2'));
// expected output: 3140
/*
But even using isNaN() is an imperfect solution.
A better solution would either be to use value !== value, which would only produce true if the value is equal to NaN.
Also, ES6 offers a new Number.isNaN() function, which is a different and more reliable than the old global isNaN() function.

************************************************************************************************************************

|| Which is faster between JavaScript and an ASP script?

JavaScript is faster.
JavaScript is a client-side language, and thus it does not need the assistance of the webserver to execute.
On the other hand, ASP is a server-side language and hence is always slower than JavaScript.
Javascript now is also a server-side language (nodejs).

The ASP programming language is used to code dynamic web pages, web services and applications.
The term ASP is an acronym for Active Server Pages.
The ASP coding language is a proprietary Microsoft product and the code must be run and interpreted
     on a Windows IIS web server.
The ASP language can include HTML tags and text, however, the “active” scripts on the page are server-side code
    and must be interpreted on the web server. However, some non-Microsoft web servers, such as Apache::ASP,
    can run ASP pages with limited capacity, according to W3 Schools.
ASP pages are returned to the client browser as a plain HTML page. Therefore, the server-side code remains private and more secure.

ASP can be used to code pages that display tailored content to different users based on a number of factors,
    such as browser type or IP address.

************************************************************************************************************************

|| What is negative Infinity?

(Negative Infinity is a number in JavaScript which can be derived by dividing negative number by zero).

In addition to representing floating-point numbers, the Number type has three symbolic values:
    +Infinity, -Infinity, and NaN ("Not a Number").
The negative infinity in JavaScript is a constant value which is used to represent a value which is the lowest available.
This means that no other number is lesser than this value. It can be generated using a self-made function
    or by an arithmetic operation.
Note: JavaScript shows the NEGATIVE_INFINITY value as -Infinity.
This value behaves slightly differently than mathematical infinity.
Because NEGATIVE_INFINITY is a static property of Number, you always use it as Number.NEGATIVE_INFINITY,
    rather than as a property of a Number object you created.

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/NEGATIVE_INFINITY

************************************************************************************************************************

|| Is it possible to break JavaScript Code into several lines?

In the past when we want to split a traditional string over multiple lines (for printing to console),
    we need to include a newline character, \n:
    Examples:
    alert("Hello\nHow are you?");
    output = 'I like the song "' + song + '".\nI gave it a score of ' + (score/highestScore * 100) + '%.';
But now template literals respect the line breaks in the source code, so newline characters are no longer needed:
    output = `I like the song "${ song }".
    I gave it a score of ${ score/highestScore * 100 }%.`;

And if you change to a new line when not within a string statement, then javaScript ignores the break in the line.
Example:
    var x=1, y=2,
    z=
    x+y;
The above code is perfectly fine, though not advisable as it hampers debugging.

************************************************************************************************************************

|| What are undeclared and undefined variables?

Undefined: It occurs when a variable has been declared but has not been assigned with any value.
    If the program tries to read the value of an undefined variable, an undefined value is returned.
    Undefined is not a keyword.

Undeclared: It occurs when we try to access any variable that is not initialized or declared earlier
    using var or const keyword.
    If we use ‘typeof’ operator to get the value of an undeclared variable, we will face the runtime error
        with return value as “undefined”.
    The scope of the undeclared variables is always global.
function GFG(){
//'use strict' verifies that no undeclared
// variable is present in our code
   'use strict';
   x = "GeeksForGeeks";
}

 GFG(); //accessing the above function

 Output: ReferenceError: x is not defined

************************************************************************************************************************

|| Write the code for adding new elements dynamically?

<html>
    <head>
        <title>t1</title>
        <script type="text/javascript">
            function addNode () {
                var newP = document. createElement("p");
                var textNode = document.createTextNode(" This is a new text node");
                newP.appendChild(textNode);
                document.getElementById("firstP").appendChild(newP);
            }
        </script>
    </head>
    <body>
        <p id="firstP">firstP<p>
    </body>
</html>

************************************************************************************************************************

|| What is difference between local and global variable?

Local Variable:
When you use JavaScript, local variables are variables that are defined within functions.
They have local scope, which means that they can only be used within the functions that define them.
Since local variables are defined inside the function so variables with the same name can be used in different functions.
The use of local variables reduces the likelihood of naming conflicts.

Global Variable:
In contrast, global variables are variables that are defined outside of functions.
These variables have global scope, so they can be used by any function without passing them to the function as parameters.
Since global variables are defined outside there function so variables with the same name can not be used in different functions.
All the scripts and functions on a web page can access it.
Global variables often create problems. That’s because any function can modify a global variable, and it’s all too easy
    to misspell a variable name or modify the wrong variable, especially in large applications.
    That, in turn, can create debugging problems.
If you forget to code the "var" keyword in a variable declaration, the JavaScript engine assumes that the variable is global.
    This can cause debugging problems.

************************************************************************************************************************

|| What is a prompt box?

JavaScript has three kind of popup boxes: Alert box, Confirm box, and Prompt box.
- alert box is often used if you want to make sure information comes through to the user.
- confirm box is often used if you want the user to verify or accept something.
- prompt box is often used if you want the user to input a value before entering a page.

To display line breaks inside a popup box, use a back-slash followed by the character n.

https://www.w3schools.com/js/js_popup.asp

************************************************************************************************************************

|| What is ‘this’ keyword in JavaScript?

https://www.javascripttutorial.net/javascript-this/

JavaScript has the this keyword that behaves differently from other programming languages.
In JavaScript, you can use the this keyword in the global and function contexts.
Moreover, the behavior of the  this keyword changes between strict and non-strict modes.
The this references the object of which the function is a property, in other words, the this references the object
    that is currently calling the function.
The value of “this” keyword will always depend on the object that is invoking the function.

In the global context
The this references the "global object", which is the "window" object on the web browser or "global" object on Node.js.
This behavior is consistent whether the strict mode is applied or not.
If you assign a property to this object in the global context, JavaScript will add the property to the global object
    as shown in the following example:
        this.color= 'Red';
        console.log(window.color); // 'Red'

In function context:
1) Simple function invocation
In the non-strict mode, the this references the "global object" when the function is called as follows:
    function show() {
       console.log(this === window); // true
    }
    show(); // = window.show();

In the strict mode, JavaScript sets the this to undefined.

2) Method invocation
When you call a method of an object, JavaScript sets this to the object that owns the method.

Next are Constructor invocation and Indirect Invocation which are more complex - see link above.

In arrow functions
JavaScript sets the this lexically.
It means the arrow function does not create its own "execution context" but inherits the this from the outer object
    where the arrow function is defined.
    - defining a method using an arrow function will cause an issue as the this value references the global object,
        not the object which owns the method.

SPECOŠKA:
const b = {
    name:"Vivek",
    f: function(){
      var self = this;
      console.log(this.name);
      (function(){
        console.log(this.name);
        console.log(self.name);
      })();
    }
  }
  b.f();
In the IIFE inside the function f, the 'this' keyword refers to the global/window object.

************************************************************************************************************************

|| What is the working of timers in JavaScript?

The window object allows execution of JS code at specified time intervals.
These time intervals are called timing events.
The two key methods to use with JavaScript are:
setTimeout(function, milliseconds)
    Executes a function, after waiting a specified number of milliseconds.
setInterval(function, milliseconds)
    Same as setTimeout(), but repeats the execution of the function continuously.
The setTimeout() and setInterval() are both methods of the HTML DOM Window object.

The clearTimeout() method stops the execution of the function specified in setTimeout().

Timers are operated within a single thread, and thus events might queue up, waiting to be executed.
(Časovače jsou provozovány v rámci jednoho vlákna, a proto se události mohou řadit do fronty a čekat na provedení.)

************************************************************************************************************************

|| Which symbol is used for comments in Javascript?

// for Single line comments

- multi-line comments which used for whole block of these interview questions

************************************************************************************************************************

|| What is the difference between ViewState and SessionState?

https://www.geeksforgeeks.org/viewstate-vs-sessionstate/

As the web is stateless, State management is done to maintain the state of a page and the server-side as well.
ViewState is to manage state at the client’s end, making state management easy for end-user.
SessionState manages state at the server’s end, making it easy to manage content from this end.

ViewState is maintained at only one level that is page-level.
Changes made on a single page is not visible on other pages.
Information that is gathered in ViewState is stored for the clients only and cannot be transferred to any other place.
ViewState has a tendency for the persistence of page-instance-specific data.
When view state is used, the values posted of a particular page persist in the browse area that the client is using
    and post back only when the entire operation is done.
The data of the previous page is no longer available when another page is loaded.
Also, Data is not secure in this case because it is exposed to clients. Encryption can be used for data security.
It can be used to store information that you wish to access from same web page.

SessionState is maintained at session-level and data can be accessed across all pages in the web application.
The information is stored within the server and can be accessed by any person that has access to the server
    where the information is stored.
SessionState has the tendency for the persistence of user-specific data and is maintained on the server-side.
This data remains available until the time that the session is completed or the browser is closed by the user.
It can be used to store information that you wish to access on different web pages.

************************************************************************************************************************

|| How you can submit a form using JavaScript?

Generally, a form is submitted when the user presses a submit button.
However, sometimes, you may need to submit the form programmatically using JavaScript.
JavaScript uses the "HTMLFormElement" object to represent a form.
The HTMLFormElement has the properties that correspond to the HTML attributes: action and method,
    and also provides the following useful methods: submit() and reset()

Use the ‘id’ of the form to get the form object.
For example, if the name of your form is ‘myform’, the JavaScript code for the submit call is:

document.forms["myform"].submit();

************************************************************************************************************************

|| Does JavaScript support automatic type conversion?

YES.
JavaScript is a "loosely typed" language, which means that whenever an operator or statement is expecting
    a particular data-type, JavaScript will automatically convert the data to that type.
"Type coercion"(= donucování) is the automatic or implicit conversion of values from one data type to another
    (such as strings to numbers).
"Type conversion" (= přeměna) is similar to type coercion because they both convert values from one data type to another
    with one key difference — type coercion is implicit whereas type conversion can be either implicit or explicit.
IMPLICITNÍ = zahrnutý, obsažený, ale nevyjádřený přímo, nikoli zjevný, samosebou se rozumějící
EXPLICITNÍ = výslovný, přímý, jasný, zřetelný; otevřeně, přímo vyjádřený, nenechávající nic zamlčeného, skrytého, k vysouzení

const value1 = '5';
const value2 = 9;
let sum = value1 + value2;

console.log(sum);

In the above example, JavaScript has coerced the 9 from a number into a string and then concatenated the two values together,
    resulting in a string of 59.
JavaScript had a choice between a string or a number and decided to use a string.
The compiler could have coerced the 5 into a number and returned a sum of 14, but it did not.

String coercion takes place while using the ‘ + ‘ operator. When a number is added to a string, the number type
    is always converted to the string type.

To return this result, you'd have to explicitly convert the 5 to a number using the Number() method:
sum = Number(value1) + value2;

Type coercion also takes place when using the ‘ - ‘ operator, but the difference while using ‘ - ‘ operator is that,
    a string is converted to a number and then subtraction takes place.

!!! Boolean Coercion
Takes place when using logical operators, ternary operators, if statements and loop checks with using truthy and falsy values.
OR ( || ) operator - If the first value is truthy, then the first value is returned.
    Otherwise, always the second value gets returned.

AND ( && ) operator - If both the values are truthy, always the second value is returned.
If both the values are falsy, always the first value is returned.
If the first value is falsy then the first value is returned or if the second value is falsy then the second value is returned.

************************************************************************************************************************

|| How can be the style/class of an element changed using JS?

The first step is to select the element you want to apply or change a style on (document.getElementById(), document.querySelector()).
After you’ve selected an element, you can change its style by attaching the style property to the selector,
    followed by the style you want to change.

document.getElementById("myText").style.fontSize = "20";
document. getElementById ("myText").className = "anyclass";

************************************************************************************************************************

|| How to read and write a file using JavaScript?

You cannot read or write files in JS on client side(browsers).
This can be done on serverside using the fs module in Node.js. It provides sync and async functions to read
    and write files on the file system.

In the context of browser, Javascript can READ user-specified file. See Eric Bidelman's blog for detail about reading
    file using File API.
    https://web.dev/read-files/
    The "File and Directory Entries API" simulates a local file system that web apps can navigate within
        and access files in. You can develop apps which read, write, and create files and/or directories
        in a virtual, sandboxed file system.
        https://developer.mozilla.org/en-US/docs/Web/API/File_and_Directory_Entries_API

However, it is not possible for browser-based Javascript to WRITE the file system of local computer without disabling
    some security settings because it is regarded as a security threat for any website to change your local file system
    arbitrarily (svévolně).

************************************************************************************************************************

|| What are all the looping structures in JavaScript?

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration

The statements for loops provided in JavaScript are:

for statement
    for...in statement
    for...of statement

do...while statement
while statement

special: labeled statement

key words used in loops:
    break statement
    continue statement

************************************************************************************************************************

||  What is for...in loop in Javascript?

for(variable in object)
      statement

- for...in iterates over property names;
- iteruje v ramci objektu a vraci jednotlive elementy objektu;
- v ramci "array" je i = indexu elementu; v ramci "object" je rovno "key" properte;

    for(i in cats) {
        console.log(i);
    }
        // 0
        // 1
        // 2
        // 3
        // 4

************************************************************************************************************************

|| What is called Variable typing in Javascript?

JavaScript is a loosely typed language; the type of a variable is determined only when a value is assigned
    and can change as the variable appears in different contexts because the type of a variable is checked during run-time
It is also called as DYNAMIC TYPING as JS is a "dynamically typed language", which means that, unlike some other languages,
    you don't need to specify what data type a variable will contain.

************************************************************************************************************************

|| How can you convert the string of any base to an integer in JavaScript?

In JavaScript parseInt() function is used to convert the string to an integer.
This function returns an integer of base which is specified in second argument of parseInt() function.
parseInt() function returns NaN when the string doesn’t contain number.

************************************************************************************************************************

|| What would be the result of 3+2+”7″?

57

************************************************************************************************************************

|| How to detect the operating system on the client machine?

To detect the operating system on the client machine, one can simply use navigator.appVersion or navigator.userAgent property.
The Navigator appVersion property is a read-only property and it returns a string which represents the version
    information of the browser.
The Window.navigator read-only property returns a reference to the Navigator object, which can be queried for information
    about the application running the script.
The navigator.appVersion string should be used to find the name of the operating system on the client.

This example uses navigator.appVersion property to display all the properties of the client machine.
<!DOCTYPE html>
<html>

<head>
	<title>
		How to detect operating system on the
		client machine using JavaScript ?
	</title>
</head>

<body style="text-align:center;">

	<h1 style="color:green;">GeeksforGeeks</h1>

	<button ondblclick="version()">
		Return OS Version
	</button>

	<p id="OS"></p>

	<!-- Script to return OS details -->
	<script>
		function version() {
			var os = navigator.appVersion;

			// Display the OS details
			document.getElementById("OS").innerHTML = os;
		}
	</script>
</body>

</html>

************************************************************************************************************************

|| What is the function of the delete operator?

It removes a property from an object.
It cannot delete a variable. Any property declared with var cannot be deleted from the global scope or from a function's scope.
On successful deletion, it will return "true", else "false" will be returned.
Deleting built-in objects like Math, Date, and window objects are unsafe, and they can crash your entire application.

const Employee = {
  firstname: 'John',
  lastname: 'Doe'
};

console.log(Employee.firstname);
// expected output: "John"

delete Employee.firstname;

console.log(Employee.firstname);
// expected output: undefined

************************************************************************************************************************

|| What is the use of "javascript:void(0)"?

https://www.freecodecamp.org/news/javascript-void-keyword-explained/

javascript:
-  it is referred to as a "Pseudo URL". When a browser receives this value as the value of href on an anchor tag,
    it interprets the JS code that follows the colon (:) rather than treating the value as a referenced path.
<a href="javascript:console.log('javascript');alert('javascript')">Link</a>

void(0)
- the void operator evaluates given expressions and returns undefined.
const result = void(1 + 1);
console.log(result);
// undefined

Sometimes, you do not want a link to navigate to another page or reload a page.
Using javascript:, you can run code that does not change the current page.
This, used with void(0) means, do nothing - don't reload, don't navigate, do not run any code.
The "Link" word is treated as a link by the browser. For example, it's focusable, but it doesn't navigate to a new page.
0 is an argument passed to void that does nothing, and returns nothing.

JavaScript code (as seen above) can also be passed as arguments to the void method.
This makes the link element run some code but it maintains the same page.

************************************************************************************************************************

|| What are escape characters?

backslash \
- putting a \ just before the character to make sure they are recognized as text, not part of the code;
    let bigmouth = 'I\'ve got no right to take my place...';
"placeholder" ${ }
- easier escaping characters available from ES6, variables are put inside a ${ } construct

************************************************************************************************************************

|| What is a pop() method in JavaScript?

It is an Array class method which removes last item from the array.

************************************************************************************************************************

|| What are the disadvantages of using innerHTML in JavaScript?

https://www.geeksforgeeks.org/what-is-the-disadvantage-of-using-innerhtml-in-javascript/

Performance and security issues.

(It sets or returns the HTML content (the inner HTML) of an element. The innerHTML property is widely used to modify
    the contents of a webpage as it is the easiest way of modifying DOM.)

- The use of innerHTML very slow: The process of using innerHTML is much slower as its contents as slowly built,
    also already parsed contents and elements are also re-parsed which takes time.
- Content is replaced everywhere: Either you add, append, delete or modify contents on a webpage using innerHTML,
    all contents is replaced, also all the DOM nodes inside that element are re-parsed and recreated.
- Preserves event handlers attached to any DOM elements: The event handlers do not get attached to the new elements
    created by setting innerHTML automatically. To do so one has to keep track of the event handlers and attach it
    to new elements manually. This may cause a memory leak on some browsers.
- Appending to innerHTML is not supported: Usually, += is used for appending in JavaScript. But on appending
    to an Html tag using innerHTML, the whole tag is re-parsed.
- Old content replaced issue: The old content is replaced even if object.innerHTML = object.innerHTML + ‘html’ is used
    instead of object.innerHTML += ‘html’. There is no way of appending without re-parsing the whole innerHTML.
    Therefore, working with innerHTML becomes very slow. String concatenation just does not scale when dynamic
    DOM elements need to be created as the plus’ and quote openings and closings becomes difficult to track.
- Can break the document: There is no proper validation provided by innerHTML, so any valid HTML code can be used.
    This may break the document of JavaScript. Even broken HTML can be used, which may lead to unexpected problems.
- Can also be used for Cross-site Scripting (XSS): The fact that innerHTML can add text and elements to the webpage,
    can easily be used by malicious users to manipulate and display undesirable or harmful elements
    within other HTML element tags. Cross-site Scripting may also lead to loss, leak and change of sensitive information.

************************************************************************************************************************

|| What is break and continue statements?

The "break" statement terminates the current loop, switch, or label statement and transfers program control
    to the statement following the terminated statement.

The "continue" statement terminates execution of the statements in the current iteration of the current or labeled loop,
    and continues execution of the loop with the next iteration.
In contrast to the "break" statement, "continue" does not terminate the execution of the loop entirely: instead,
    In a while loop, it jumps back to the condition.
    In a for loop, it jumps to the update expression.

************************************************************************************************************************

|| What are JavaScript Cookies?

https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie
https://www.w3schools.com/js/js_cookies.asp

Cookies let you store user information in web pages.
The Document property "cookie" lets you read and write cookies associated with the document.
It serves as a getter and setter for the actual values of the cookies.
When a web server has sent a web page to a browser, the connection is shut down, and the server forgets
    everything about the user.
Cookies were invented to solve the problem "how to remember information about the user":

When a user visits a web page, its name can be stored in a cookie.
Next time the user visits the page, the cookie "remembers" the name.

Cookies are saved in name-value pairs like:
When a browser requests a web page from a server, cookies belonging to the page are added to the request.
This way the server gets the necessary data to "remember" information about users.

document.cookie will return all cookies in one string much like: cookie1=value; cookie2=value; cookie3=value;
document.cookie = "username=John Smith; expires=Thu, 18 Dec 2013 12:00:00 UTC; path=/";

************************************************************************************************************************

|| What are basic groups of data types in JavaScript?

There are 8 data types in JavaScript which can be divided into three main categories:

primitive (or primary) - String, Number, BigInt, Symbol and Boolean - can hold only one value at a time
    - special - Undefined and Null
composite (or reference) - Object, Array, and Function (which are all types of objects) - can hold collections of values
    and more complex entities

************************************************************************************************************************

|| What is generic object and how can it be created?

When JavaScript sees the "new" operator, it creates a new generic object and implicitly sets the value
    of the internal property [[Prototype]] to the value of constructor function prototype.

The "new" operator creates a generic object and sets its __proto__ property to constructor function prototype.

var I = new Object();

************************************************************************************************************************

|| What is the use of a "typeof" operator?

You can use the typeof operator to find the data type of a JavaScript variable.
The typeof operator returns a string indicating the type of the unevaluated operand.

zajimavosti:
typeof null === 'object';
typeof([]) === 'object';
typeof document.all === 'undefined';

************************************************************************************************************************

|| Which keywords are used to handle exceptions?

JavaScript handles exceptions in "try-catch-finally" statements and "throw" statements.
The try statement lets you test a block of code for errors.
The catch statement lets you handle the error.
The throw statement lets you create custom errors.
The finally statement lets you execute code, after try and catch, regardless of the result.

If you use throw together with try and catch, you can control program flow and generate custom error messages.

************************************************************************************************************************

|| Which keyword is used to print the text on the screen?

document.write(“Welcome”) is used to print the text "Welcome" on the screen.

************************************************************************************************************************

|| What is the use of the blur() function?

The HTMLElement.blur() method removes keyboard focus from the current element.

************************************************************************************************************************

|| What are the different types of errors in JavaScript?

There are three main types of errors that can occur while compiling a JavaScript program:

syntax errors, also known as parsing errors -  Occurs when there is a mistake in the way the code is written;
    for example, a typo or missing character.

runtime errors - Occurs when the script is unable to complete its instructions; for example, if a specified
    object cannot be found.

logical errors

************************************************************************************************************************

|| What’s the difference between JavaScript and JScript?

JScript is same of JavaScript as JScript was the variant of Microsoft’s JavaScript.
It’s only support Microsoft Internet Explorer.

************************************************************************************************************************

|| What are the ways of setting (update) the value of object members?

You can use dot or bracket notation.

person.age = 45;
person['eyes'] = 'hazel';

************************************************************************************************************************

|| What is the ‘Strict Mode in JavaScript, and how can it be enabled?

JavaScript's strict mode, introduced in ECMAScript5, is a way to opt in to a restricted variant of JavaScript.
Strict mode can be enabled by adding the string literal “use strict”.
Strict mode applies to entire scripts or to individual functions.

Strict mode makes it easier to write "secure" JavaScript.

Strict mode changes previously accepted "bad syntax" into real errors - eliminates some JavaScript silent errors
    by changing them to throw errors.
    As an example, in normal JavaScript, mistyping a variable name creates a new global variable.
        In strict mode, this will throw an error, making it impossible to accidentally create a global variable.
Fixes mistakes that make it difficult for JavaScript engines to perform optimizations: strict mode code can sometimes
    be made to run faster than identical code that's not strict mode.
Eliminates 'this' coercion. Without strict mode, a reference to a 'this' value of 'null' or 'undefined' is automatically
    coerced to the global.

Prohibits some syntax likely to be defined in future versions of ECMAScript.

Sometimes you'll see the default, non-strict mode referred to as "sloppy mode" (nedbalý).

************************************************************************************************************************

|| What is the way to get the status of a CheckBox?

First, select the checkbox using the selecting DOM methods such as getElementById() or querySelector().
Then, access the checked property of the checkbox element. If its "checked" property is "true", then the "checkbox"
    is checked; otherwise, it is not.

<label for="accept">
   <input type="checkbox" id="accept" name="accept" value="yes">  Accept
</label>
console.log(document.getElementById('accept').checked);

************************************************************************************************************************

|| What is a window.onload and document.onload?

window.onload:
The window object represents the browser window.
The onload property processes load events after the element has finished loading.
This is used with the window element to execute a script after the webpage has completely loaded.
The function that is required to be executed is assigned as the handler function to this property.
It will run the function as soon as the webpage has been loaded.

window.onload = function exampleFunction() {
      // Function to be executed
}

document.onload
It is called when the DOM tree (built from the markup code within the document) is completed and ready
    which can be prior to images and other external content is loaded.

window.onload appears to be the most widely supported. In fact, some of the most modern browsers have in a sense
    replaced document.onload with window.onload. Browser support issues are most likely the reason why many people
    are starting to use libraries such as jQuery to handle the checking for the document being ready, like so:

$(document).ready(function() { code here });
$(function() { code here  });

************************************************************************************************************************

|| How to append an item to an array?

Mutative way (will change the original array): push, unshift, splice, and length

array.push('a');
array.unshift(0);
array.splice(array.length, 0, 'b');
array[array.length] = 'c';

Non Mutative (will create a new array and the original array remains unchanged): concat, and spread operator

const originalArray = ['a'];
let newArray;

newArray = originalArray.concat('b');
OR
newArray = [...originalArray, 'b'];

************************************************************************************************************************

|| What is the Anonymous function in JavaScript?

It is a function that does not have any name associated with it.
An anonymous function is not accessible after its initial creation, it can only be accessed by a variable
    it is stored in as a function as a value.
const myGreeting = function() {
      alert('hello');
    }
    myGreeting();

    This form of creating a function is also known as "function expression".
    We can declare it using Arrow Function as well.

Generally use an anonymous function along with an event handler:

    const myButton = document.querySelector('button');
    myButton.onclick = function() {
      alert('hello');
    }

Another use case of anonymous functions is IIFE (immediately invoked function expression)
    also known as Self Executing Function.

    (function () {
        console.log("Welcome to GeeksforGeeks!");
    })();

************************************************************************************************************************

|| What is the difference between .call() and .apply()?

With these methods you can write a method that can be used on different objects.
They can be used to invoke a method with an owner object as an argument (parameter).
With them, an object can use a method belonging to another object.

The only difference is call() method takes the arguments separated by comma
    while apply() method takes the array of arguments.

example:
function saySomething(message){
  return this.name + " is " + message;
}
var person4 = {name:  "John"};
saySomething.call(person4, "awesome"); // Returns "John is awesome"

Since JavaScript arrays do not have a max() method, you can apply the Math.max() method instead.
Math.max.apply(null, [1,2,3]); // Will return 3

************************************************************************************************************************

|| What is the bind() method?

It returns a new function, where the value of “this” keyword will be bound to the owner object,
    which is provided as a parameter.

example:
var bikeDetails = {
    displayDetails: function(registrationNumber,brandName){
    return this.name+ " , "+ "bike details: "+ registrationNumber + " , " + brandName;
  }
}
var person1 = {name:  "Vivek"};

var detailsOfPerson1 = bikeDetails.displayDetails.bind(person1, "TS0122", "Bullet"); // Binds the displayDetails function to the person1 object

detailsOfPerson1(); // Returns "Vivek, bike details: TS0452, Thunderbird"

example:
const module = {
  x: 42,
  getX: function() {
    return this.x;
  }
};

const unboundGetX = module.getX;
console.log(unboundGetX()); // The function gets invoked at the global scope - expected output: undefined

const boundGetX = unboundGetX.bind(module);
console.log(boundGetX()); // expected output: 42

************************************************************************************************************************

|| What is event bubbling?

It is a method of event propagation (= šíření událostí) in the HTML DOM API.
When an event is in an element inside another element, and both elements have registered a handle to that event.
It is a process that starts with the element that triggered the event and then bubbles up to the containing elements
    in the hierarchy.
In event bubbling, the event is first captured and handled by the innermost element and then propagated to outer elements.

This is a very annoying behavior, but there is a way to fix it - standard Event object has a function available on it
    called stopPropagation() which, when invoked on a handler's event object, makes it so that first handler is run
    but the event doesn't bubble any further up the chain, so no more handlers will be run;

By default all event handlers are registered in the bubbling phase (not in capturing phase).

************************************************************************************************************************

|| Is JavaScript case sensitive? Give an example.

YES.
If we wrote parseInt() it would be something different from Parseint() which will not work.

************************************************************************************************************************

|| What logical operators can be used in JavaScript?

-- && — AND:
    - allows you to chain together two or more expressions so that all of them have to individually
    evaluate to true for the whole expression to return true;
-- || — OR:
    - allows you to chain together two or more expressions so that one or more of them have to individually evaluate
     to true for the whole expression to return true;
-- ! — NOT:
    - can be used to negate an expression;

************************************************************************************************************************

|| What are the differences between Web Garden and a Web Farm in JavaScript?

Both are the web hosting systems.

Web Garden consists of a single server on which any number of processes can be run. It consists of multiple “processes”.
Web Farm has the ability to runs across multiple servers. It consists of multiple “computers”.

************************************************************************************************************************

|| How closures work in JavaScript?

A closure is a function having access to the parent scope, even after the parent function has closed.
It makes it possible for a function to have "private" variables.
In JavaScript, closures are created every time a function is created, at function creation time.
A closure is the combination of a function bundled together (enclosed) with references to its surrounding state
    (the lexical environment) - A closure is the combination of a function and the lexical environment
        within which that function was declared.
This environment consists of any local variables that were in-scope at the time the closure was created.
In other words, a closure gives you access to an outer function’s scope from an inner function.
They let you associate data (the lexical environment) with a function that operates on that data.
Changes to the variable value in one closure don't affect the value in the other closure.

Every closure has three scopes:

Local Scope (Own scope)
Outer Functions Scope
Global Scope

(Kod mam ulozen i v souboru playground.html)
*/
function makeFunc() {
    var name = 'Mozilla';
    function displayName() {
      alert(name);
    }
    return displayName;
  }

  var myFunc = makeFunc();
  myFunc();
/*
- 'myFunc' is a reference to the instance of the function 'displayName'
- 'displayName' is created when 'makeFunc' is run
-  instance of 'displayName' maintains a reference to its lexical environment, within which the variable 'name' exists
-  for this reason, when 'myFunc' is invoked, the variable 'name' remains available for use, and "Mozilla" is passed to alert

EXAMPLE
These closures follow the Module Design Pattern (described in the following question)
*/
var makeCounter = function() {
  var privateCounter = 0;
  function changeBy(val) {
    privateCounter += val;
  }
  return {
    increment: function() {
      changeBy(1);
    },

    decrement: function() {
      changeBy(-1);
    },

    value: function() {
      return privateCounter;
    }
  }
};

var counter1 = makeCounter();
var counter2 = makeCounter();

alert(counter1.value());  // 0.

counter1.increment();
counter1.increment();
alert(counter1.value()); // 2.

counter1.decrement();
alert(counter1.value()); // 1.
alert(counter2.value()); // 0.
/*
************************************************************************************************************************

|| What is Module Pattern in JS? How does it look like?

https://addyosmani.com/resources/essentialjsdesignpatterns/book/#modulepatternjavascript

The Module Pattern is one of the important patterns in JavaScript.
It is a commonly used Design Pattern which is used to wrap a set of variables and functions together in a single scope.

It uses 'object literals' (the way of creation of objects in JS) but only as the return value from a scoping function.


It is used to define objects and specify the variables and the functions that can be accessed from outside the scope
    of the function.
We expose certain properties and function as public and can also restrict the scope of properties and functions
    within the object itself, making them private. This means that those variables cannot be accessed outside
    the scope of the function.

The Module pattern encapsulates "privacy", state and organization using closures.
(Variables can't technically be declared as being 'public' nor 'private' and so we use function scope to simulate this concept.)
Within the Module pattern, variables or methods declared are only available inside the module itself thanks to closure.
    Variables or methods defined within the returning object however are available to everyone.

************************************************************************************************************************

|| How are DOM utilized in JavaScript?

The Document Object Model (DOM) is a programming interface (API) for web (HTML and XML) documents.
It represents the page.
The DOM represents the document as nodes and objects; that way, programming languages can interact with the page and
    can change the document structure, style, and content.
DOM is built using multiple APIs that work together, e.g. the "HTML DOM API" adds support for representing HTML documents
    to the core DOM.
The DOM is not a programming language, but without it, the JavaScript language wouldn't have any model or notion of web pages.
The DOM is not part of the JavaScript language, but is instead a Web API used to build websites.

************************************************************************************************************************

|| How are event handlers utilized in JavaScript?

EVENTS are actions or occurrences that happen in the system, which the system tells you about so you can respond to them
    in some way if desired;
- EVENT HANDLERS: each available event has an EVENT HANDLER, which is a block of code (usually a JS function
    that you as a programmer create) that runs when the event fires;
- when such a block of code is defined to run in response to an event, we say we are "registering an event handler";
- EVENT LISTENERS listen out for the event happening (and the handler is the code that is run in response to it happening);
- Web events are not part of the core JS language — they are defined as part of the APIs built into the browser;

EVENT HANDLER PROPERTIES
= are the properties that exist to contain event handler code;
- e.g. "onclick" is a property like any other available on the button element but it is a special type — when you set it
    to be equal to some code, that code is run when the event fires on the button;

const btn = document.querySelector('button');

btn.onclick = function() {
  const rndCol = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
  document.body.style.backgroundColor = rndCol;
}

The modern mechanism for adding event handlers is the addEventListener() method.

Specošky:
- method: stopPropagation() - stop bubbling phase done
"event delegation" - vyuziti bubblingu pro ucely spusteni udalosti na rodicovskem elementu

************************************************************************************************************************

|| What is the role of deferred scripts in JavaScript?

It is a part of SCRIPT LOADING STRATEGIES.
The code won't work if the JS is loaded and parsed before the HTML you are trying to do something to.
For the external JS, we use a modern JavaScript feature - the "defer" attribute - - "defer" only works for external scripts.
It tells the browser to continue downloading the HTML content once the <script> tag element has been reached.
it specifies that the script is downloaded in parallel to parsing the page, and executed after the page has finished parsing.

************************************************************************************************************************

|| What are Screen objects?

The screen object contains information about the visitor's screen.

Screen Object Properties:

availHeight	Returns the height of the screen (excluding the Windows Taskbar)
availWidth	Returns the width of the screen (excluding the Windows Taskbar)
colorDepth	Returns the bit depth of the color palette for displaying images
height	Returns the total height of the screen
pixelDepth	Returns the color resolution (in bits per pixel) of the screen
width	Returns the total width of the screen

************************************************************************************************************************

|| What is unescape() and escape() functions?

escape() function:
takes a string as a single parameter & encodes the string that can be transmitted over the computer network which s
    upports ASCII characters. Encoding is the process of converting plain text into ciphertext.
(Tato funkce bere řetězec jako jediný parametr a kóduje řetězec, který lze přenášet přes počítačovou síť,
    která podporuje znaky ASCII. Kódování je proces převodu prostého textu na šifrovaný text.)

unescape() function:
takes a string as a single parameter and uses it to decode that string encoded by the escape() function.
The hexadecimal sequence in the string is replaced by the characters they represent when decoded via unescape() function.

Both are almost deprecated. Not for use in new websites.

************************************************************************************************************************

|| What are the decodeURI() and encodeURI()?

encodeURI() encodes a URI (= Uniform Resource Identifier)
decodeURI() decodes a URI after encoding it.

const uri = 'https://mozilla.org/?x=шеллы';
const encoded = encodeURI(uri);
console.log(encoded);
// expected output: "https://mozilla.org/?x=%D1%88%D0%B5%D0%BB%D0%BB%D1%8B"

try {
  console.log(decodeURI(encoded));
  // expected output: "https://mozilla.org/?x=шеллы"
} catch (e) { // catches a malformed URI
  console.error(e);
}

************************************************************************************************************************

|| What does the following statement declare?
var myArray = [[[]]];

It declares a three-dimensional array.

************************************************************************************************************************

|| What is namespacing in JavaScript, and how is it used?

Namespace refers to providing scope to the identifiers (names of types, functions, variables, etc) to prevent
    collisions between them.
Same variable name might be required in different contexts. Using namespaces will isolate these contexts.

Namespacing is the act of wrapping a set of entities, variables, functions, objects under a single umbrella term.
(je akt zabalení množiny entit, proměnných, funkcí, objektů pod jeden zastřešující termín)
(umbrella term = term used to cover a broad (široký) number of functions or items that all fall under
    a single common category - a category under which disparate (nesourodý, rozličný) items are grouped)

We can use the same identifier in different namespaces by attaching it to different global objects:

var car = {
    startEngine: function () {
        console.log("Car started");
    }
}

var bike = {
    startEngine: function () {
        console.log("Bike started");
    }
}

car.startEngine();
bike.startEngine();

SUB-NAMESPACES:
person {
    name : {
      first: 'Bob',
      last: 'Smith'
}}
-> this is a sub-namespace (object within object): person.name.first;
************************************************************************************************************************

|| How to hide JS code from old browsers that do not support JavaScript?

This question has become far less relevant now that virtually all browsers support JavaScript.
Nevertheless, to prevent old browsers from displaying your JS code, you can do the following:

Immediately after the opening <script> tag, put a one-line HTML-style comment without the closing characters,
    so that the first two lines of your script would look like this:
<script type="text/javascript" language="JavaScript">
<!--
At the end of your script, put the following two lines:
//-->
</script>

Thus, your HTML file will contain the following fragment:

<script language="JavaScript">
<!--
Here you put your JS code.
Old browsers will treat it
as an HTML comment.
//-->
</script>

Old browsers will treat your JS code as one long HTML comment.
On the other hand, new JavaScript-aware browsers will normally interpret JS code between the tags <script> and </script>
    (the first and last lines of your JS code will be treated by the JavaScript interpreter as one-line comments).

************************************************************************************************************************

|| How can we apply unit testing in JS?

Unit testing is the process of testing the implemented code at a module level.
Unit testing allows you to ensure that your developed modules are meeting the requirements specified
    by the business document.
These tests are written for every module as they are created.
After every new module development, the entire suite of test cases is run to ensure that no existing modules
    are affected by the developed module.

JS unit testing tools: MochaJS, Jest, Jasmine, ...

************************************************************************************************************************

|| Sorting Algorithms in JavaScript

https://www.section.io/engineering-education/sorting-algorithms-in-js/

************************************************************************************************************************

|| Does JavaScript have a compiler?

NO.
No. JS is an interpreted language, so it usually does not have a compiler.
JS is interpreted by the browsers which have their own 'JavaScript Engines'.

However, in the recent times, JS engines have become compilers.
For example, Google's V8 engine (the JS engine on which server-side JavaScript - NODE.js is based on) complies
    your JavaScript code into machine instructions instead of traditional interpretation.

************************************************************************************************************************

|| Explain Hoisting in JS.

Hoisting is a default behaviour of javascript where all the variable and function declarations are moved on top.
This means that irrespective of where the variables and functions are declared, they are moved on top of the scope.
    The scope can be both local and global.
Note - To avoid hoisting, you can run javascript in strict mode.

************************************************************************************************************************

|| Explain passed by value and passed by reference.

In JavaScript, primitive data types are passed by value and non-primitive data types are passed by reference.
In the background, when a variable is initialized, the assign operator allocates some space in the memory, stores the value
    and returns the location of the allocated memory space.

passed by value:
var y = 234;
var z = y;
The assign operator takes the value of y (234) and allocates a new space in the memory and returns the address.
Therefore, variable z is not pointing to the location of variable y, instead it is pointing to a new location in the memory.
Instead of just assigning the same address to another variable, the value is passed (předána) and new space of memory is created.

passed by reference:
var obj = { name: "Vivek", surname: "Bisht" };
var obj2 = obj;
The assign operator directly passes the location of the variable obj to the variable obj2, directly passes the address (reference).
In other words, the reference of the variable obj is passed to the variable obj2.

https://www.interviewbit.com/javascript-interview-questions/#pass-by-value-pass-by-reference

************************************************************************************************************************

|| What is an Immediately Invoked Function in JavaScript?

IIFE (pronounced as IIFY) is a function that runs as soon as it is defined.

(function(){
  // Do something;
})();

First set of parenthesis tells the compiler that the function is not a function declaration, instead, it’s a function expression
(if used without these parenthesis compiler throws error there is missing name of the function for function declaration).

Second set of parenthesis is used to invoke the function.

************************************************************************************************************************

|| Explain Higher Order Functions in javascript.

= functions that operate on other functions, either by taking them as arguments or by returning them

function higherOrder(fn) {
  fn();
}
higherOrder(function() { console.log("Hello world") });

nebo

function higherOrder2() {
  return function() {
    return "Do something";
  }
}
var x = higherOrder2();
x()   // Returns "Do something"

************************************************************************************************************************

|| What is First Class Citizen in JavaScript?

If a programming language has the ability to treat functions as values (function can be stored as a value in a variable),
    to pass them as arguments and to return a function from another function then it is said that programming language
    has "First Class Functions" and these functions are called as "First Class Citizens" in that programming language.

************************************************************************************************************************

|| What is currying in JavaScript?

Currying is an advanced technique.
If we have a function f(a,b), then the function after currying, will be transformed to f(a)(b).
By using the currying technique, we do not change the functionality of a function, we just change the way it is invoked.

*/
function multiply(a,b){
  return a*b;
}
multiply(4, 3); // Returns 12

function currying(fn){
  return function(a){
    return function(b){
      return fn(a,b);
    }
  }
}
var curriedMultiply = currying(multiply);
curriedMultiply(4)(3); // Also returns 12
/*
************************************************************************************************************************

|| What is scope and scope chain in JavaScript?

Scope in JS, determines the accessibility of variables and functions at various parts in the code.
Scope is a concept that refers to where values and functions can be accessed.

Various scopes include:

Global scope (a value/function in the global scope can be used anywhere in the entire program)
File or module scope (the value/function can only be accessed from within the file)
Local / Function scope (only visible within the function),
(Code) Block scope (only visible within a { ... } codeblock); the variables declared using let and const have block scope.
    Variables declared with var do not have block scope.

JS uses global and function scope (which is local scope), and block scope to a certain extent.

Scope Chain
In Javascript, there also exists Lexical scope meaning that there is a concept of parent and child scopes.
    Each of the child scopes can access the anything defined in the parent scope (but parent can not access anything
    from it’s child scope).
    BUT - if a variable was declared in both a child as well as it’s parent, then the variable in their current scope
        will be preferred and not the one from the parent scope.
        Parent scope is checked only if a variable being used is not found in the current scope.

TEST
var parent = function () {
    var name = 'Parent';
    var child = function () {
        // What will be the output here?
        alert( name );
        var name = 'Child'
        // What will be the output here?
        alert( name );
    }
}

What is output?

// Undefined
// Child
Because of hoisting.

************************************************************************************************************************

|| What are object prototypes?

Prototypes are the mechanism by which JS objects inherit features from one another.
"prototype chain" = object can have a "prototype object", which acts as a template object (blueprint) that it inherits
    methods and properties from; object's prototype object may also have a prototype object, which it inherits
    methods and properties from, etc., etc,...
We have to distinguish between:
    - an object's prototype (which is an object from which the features are inherited)
    - an object's prototype property (which is the blueprint for objects down in prototype chain, other words if this
        object will be a constructor function then its prototype property is template for its instances)
On top of the chain is Object.prototype. Every prototype inherits properties and methods from the Object.prototype.
Example is some array on which we can use push() method without declaring such method on this concrete array as it
    inherits the method from Array.prototype.

************************************************************************************************************************

|| What are callbacks?

Functions that are used as an argument to another function are called callback functions.
A callback is a function that will be executed after another function gets executed.
    The 'divideByHalf' and 'multiplyBy2' functions are used as callback functions in the code below.
    These callback functions will be executed only after the function 'operationOnSum' is executed.

unction divideByHalf(sum){
  console.log(Math.floor(sum / 2));
}

function multiplyBy2(sum){
  console.log(sum * 2);
}

function operationOnSum(num1,num2,operation){
  var sum = num1 + num2;
  operation(sum);
}

operationOnSum(3, 3, divideByHalf); // Outputs 3
operationOnSum(5, 5, multiplyBy2); // Outputs 20

************************************************************************************************************************

|| What is memoization?

Memoization is a form of caching of values. Cache = store away in hiding or for future use.
The return value of a function is cached based on its parameters: if the parameter of that function is not changed,
    the cached version of the function is returned.

EXAMPLE

usual fce:
function addTo256(num){
  return num + 256;
}
addTo256(20); // Returns 276
addTo256(40); // Returns 296
addTo256(20); // Returns 276

When we are calling the function 'addTo256' again with the same parameter (“20” in the case above),
    we are computing the result again for the same parameter.
If the function does some heavy duty work, then, computing the result again and again with the same parameter
    will lead to wastage of time.
This is where memoization comes in, by using memoization we can store(cache) the computed results based on the parameters.
If the same parameter is used again while invoking the function, instead of computing the result, we directly
    return the stored (cached) value.

memoized fce:
function memoizedAddTo256(){
  var cache = {};

  return function(num){
    if(num in cache){
      console.log("cached value");
      return cache[num]

    }
    else{
      cache[num] = num + 256;
      return cache[num];
    }
  }
}

var memoizedFunc = memoizedAddTo256();

memoizedFunc(20); // Normal return
memoizedFunc(20); // Cached return

Although using memoization saves time, it results in larger consumption of memory since we are storing all the computed results.

************************************************************************************************************************

|| What is recursion in a programming language?

Recursion is a process of calling itself. A function that calls itself is called a recursive function.
Rekurze je technika pro opakování operace tak, že se funkce opakovaně volá, dokud nedojde k výsledku.
A recursive function must have a condition to stop calling itself. Otherwise, the function is called indefinitely.
Once the condition is met, the function stops calling itself. This is called a base condition.

EXAMPLE1
function add(number) {
  if (number <= 0) {
    return 0;
  } else {
    return number + add(number - 1);
  }
}
add(3);
=> 3 + add(2)
          3 + 2 + add(1)
          3 + 2 + 1 + add(0)
          3 + 2 + 1 + 0 = 6

EXAMPLE2
function computeSum(arr){
  if(arr.length === 1){
    return arr[0];
  }
  else{
    return arr.pop() + computeSum(arr);
  }
}

computeSum([7, 8, 9, 99]); // Returns 123

V playground.html je priklad vyuziti rekurze na nalezeni faktorialu cisla.

Advantages of recursion
1. The code may be easier to write.
2. To solve such problems which are naturally recursive such as tower of Hanoi.
3. Reduce unnecessary calling of function.
4. Extremely useful when applying the same solution.
5. Recursion reduce the length of code.
6. It is very useful in solving the data structure problem.

Disadvantages of recursion
1. Recursive functions are generally slower than non-recursive function.
2. It may require a lot of memory space to hold intermediate results on the system stacks.
3. Hard to analyze or understand the code.
4. It is not more efficient in terms of space and time complexity.
5. The computer may run out of memory if the recursive calls are not properly checked.

************************************************************************************************************************

|| What is the use of a constructor function in javascript?

Constructor functions are used to create multiple objects having similar properties and methods.
JS uses these special functions (constructor functions) to define and initialize objects and their features.
They are JS's version of a class (from ES6 there are classes as well).

// Definition of constructor function
function Person(name,age,gender){
  this.name = name;
  this.age = age;
  this.gender = gender;
}

// Creation of instances of constructor function above
var person1 = new Person("Vivek", 76, "male");
var person2 = new Person("Courtney", 34, "female");

************************************************************************************************************************

|| What are arrow functions?

They provide us with a new and shorter syntax for declaring functions (introduced in ES6).
Arrow functions can only be used as a function expression.
The biggest difference between the traditional function expression and the arrow function, is the handling of the
    'this' keyword.

var arrowAdd = (a,b) => a + b;
var arrowMultiplyBy2 = num => num * 2;

let factorial = x => {
    if (x === 0) {
        return 1;
    }
    else {
        return x * factorial(x - 1);
    }
}

************************************************************************************************************************

|| Differences between declaring variables using var, let and const

With the ES6 Version, keywords let and const were introduced to declare variables.

In the global context, a variable declared using "var" is added as a non-configurable property of the global object,
    they can be accessed using 'window.variableName'.
"var" declarations, wherever they occur, are processed before any code is executed - this is called "hoisting"
( = declaring a variable anywhere in the code is equivalent to declaring it at the top of the function or global code).
Var does not have block scope, so although it is declared within block scope it can be accessed outside this scope.

This is not valid for let/const any more - no hoisting, not added as property to global object, valid block scope.
Furthermore, variables declared with the const keyword cannot be reassigned (an initializer for a constant is required
    - you must specify its value in the same statement in which it's declared (as it can't be changed later).

************************************************************************************************************************

|| What is the rest parameter and spread operator?

Both are valid from ES6.

Rest parameter ( … )
= improved way of handling parameters of a function.
Using the rest parameter syntax, we can create functions that can take a variable number of arguments.
Any number of arguments will be converted into an array using the rest parameter.
It also helps in extracting all or some parts of the arguments.
Rest parameter should always be used at the last parameter of a function.

EXAMPLE1
function extractingArgs(...args){
  return args[1];
}
extractingArgs(8,9,1); // Returns 9

EXAMPLE2
function extractingArgs(name, city, ...args){
    return console.log(`Hi ${name} from ${city}. You can buy ${args[1]} beers.`);
}
extractingArgs('Emil', 'Prague', 1, 2, 3);

Spread operator (…)
Syntax of spread operator is exactly the same as the rest parameter, but it is used to spread an array, and object literals.
We also use spread operators where one or more arguments are expected in a function call.

EXAMPLE1
function addFourNumbers(num1,num2,num3,num4){
  return num1 + num2 + num3 + num4;
}
let fourNumbers = [5, 6, 7, 8];
addFourNumbers(...fourNumbers); // Spreads [5,6,7,8] as 5,6,7,8

EXAMPLE2
let obj1 = {x:'Hello', y:'Bye'};
let clonedObj1 = {...obj1}; // Spreads and clones obj1

Key differences between rest parameter and spread operator:
Rest parameter is used to take a variable number of arguments and turns into an array
    while the spread operator takes an array or an object and spreads it
Rest parameter is used in function declaration
    whereas the spread operator is used in function calls.

************************************************************************************************************************

|| What is the use of promises in javascript?

Promises are used to handle asynchronous operations (like server requests, working with 3rd party's API) in javascript.
A promise is created using the Promise constructor which takes in a callback function with two parameters:
    resolve and reject, respectively.
    resolve is a function that will be called, when the async operation has been successfully completed.
    reject is a function that will be called, when the async operation fails or if some error occurs.

Promise object has four states:
Pending - Initial state of promise. This state represents that the promise has neither been fulfilled nor been rejected,
    it is in the pending state.
Fulfilled - This state represents that the promise has been fulfilled, meaning the async operation is completed.
Rejected - This state represents that the promise has been rejected for some reason, meaning the async operation has failed.
Settled - This state represents that the promise has been either rejected or fulfilled.

We can consume any promise by attaching then() and catch() methods to the consumer.
    then() method is used to access the result when the promise is fulfilled.
    catch() method is used to access the result/error when the promise is rejected.

EXAMPLE

function sumOfThreeElements(...elements){
  return new Promise((resolve,reject)=>{
    if(elements.length > 3 ){
      reject("Only three elements or less are allowed");
    }
    else{
      let sum = 0;
      let i = 0;
      while(i < elements.length){
        sum += elements[i];
        i++;
      }
      resolve("Sum has been calculated: "+sum);
    }
  })
}

// consuming the promise:
sumOfThreeElements(4, 5, 6)
.then(result=> console.log(result))
.catch(error=> console.log(error));
// In the code above, the promise is fulfilled so the then() method gets executed

sumOfThreeElements(7, 0, 33, 41)
.then(result => console.log(result))
.catch(error=> console.log(error));
// In the code above, the promise is rejected hence the catch() method gets executed

************************************************************************************************************************

|| What are classes in javascript?

Introduced in the ES6 they provide a new way (nicer, easier) of declaring constructor functions in javascript.
Unlike functions, classes are not hoisted. A class cannot be used before it is declared.
A class can inherit properties and methods from other classes by using the extend keyword.
All the syntaxes inside the class must follow the strict mode(‘use strict’) of javascript. Error will be thrown
    if the strict mode rules are not followed.

class Student{
  constructor(name,rollNumber,grade,section){
    this.name = name;
    this.rollNumber = rollNumber;
    this.grade = grade;
    this.section = section;
  }

  // Methods can be directly added inside the class
  getDetails(){
    return 'Name: ${this.name}, Roll no: ${this.rollNumber}, Grade:${this.grade}, Section:${this.section}';
  }
}

let student2 = new Student("Garry", 673, "7th", "C");
student2.getDetails(); // Returns Name: Garry, Roll no:673, Grade: 7th, Section:C

************************************************************************************************************************

|| What are generator functions?

Introduced in ES6 version, generator functions are a special class of functions.
They can be stopped midway and then continue from where it had stopped.
Generator functions are declared with the function* keyword instead of the normal function keyword:
    function* genFunc(){
      // Perform operation
    }
In normal functions, we use the return keyword to return a value and as soon as the return statement gets executed,
    the function execution stops.
In the case of generator functions, when called, they do not execute the code, instead they return a generator object.
    This generator object handles the execution.
        function* genFunc(){
          yield 3;
          yield 4;
        }
        genFunc(); // Returns Object [Generator] {}
The generator object consists of a method called next() , this method when called, executes the code until the nearest
    yield statement, and returns the yield value. For example if we run the next() method on the above code:
        genFunc().next(); // Returns {value: 3, done:false}
The 'next' method returns an object consisting of 'value' and 'done' properties.
    Value property represents the yielded value.
    Done property tells us whether the function code is finished or not. Returns true if finished.

Generator functions are used to return iterators. Let’s see an example where an iterator is returned:

function* iteratorFunc() {
  let count = 0;
  for (let i = 0; i < 2; i++) {
      count++;
      yield i;
  }
  return count;
}

let iterator = iteratorFunc();
console.log(iterator.next()); // {value:0,done:false}
console.log(iterator.next()); // {value:1,done:false}
console.log(iterator.next()); // {value:2,done:true}

************************************************************************************************************************

|| What is a Map in JS?

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map

The Map object holds key-value pairs and remembers the original insertion order of the keys.
Introduced in ES6.
Any value (both objects and primitive values) may be used as either a key or a value.
A Map object iterates its elements in insertion order — a for...of loop returns an array of [key, value] for each iteration.

Differences between Objects and Maps
- Map does not contain any keys by default. It only contains what is explicitly put into it.
    Object has a prototype, so it contains default keys that could collide with your own keys if you're not careful.
- A Map's keys can be any value (including functions, objects, or any primitive).
    The keys of an Object must be either a String or a Symbol.
- The keys in Map are ordered in a simple, straightforward way: A Map object iterates entries, keys, and values
    in the order of entry insertion.
        Although the keys of an ordinary Object are ordered now, this was not always the case, and the order is complex.
        As a result, it's best not to rely on Object's property order.
- The number of items in a Map is easily retrieved from its size property.
    The number of items in an Object must be determined manually.
- A Map is an iterable, so it can be directly iterated.
    Object does not implement an iteration protocol, and so objects are not directly iterable using
        the JavaScript for...of statement (by default).
- Maps perform better in scenarios involving frequent additions and removals of key-value pairs.
    Objects are not optimized for frequent additions and removals of key-value pairs.
- No native support for serialization or parsing (must be done by ourselves).
    Native support for Objects' serialization and parsing using JSON.stringify() and JSON.parse().

The correct usage for storing data in the Map is through the set(key, value) method.

const contacts = new Map()
contacts.set('Jessie', {phone: "213-555-1234", address: "123 N 1st Ave"})

************************************************************************************************************************

|| What is a Set in JS?

The Set object lets you store unique values of any type, no element can be repeated.
Set objects are collections of values. You can iterate through the elements of a set in insertion order.
A value in the Set may only occur once.
The Set 'has' method checks if a value is in a Set object, using an approach that is quicker than testing
    most of the elements that have previously been added to the Set object.

var set1 = new Set(["sumit","sumit","amit","anil","anish"]);
// it contains ["sumit","amit","anil","anish"]

var set2 = new Set("fooooooood");
// it contains [10, 20, 30, 40]

************************************************************************************************************************

|| How the garbage collection is used in JS?

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Memory_Management

Some high-level languages, such as JavaScript, utilize a form of automatic memory management known as
    garbage collection (GC).
The purpose of a garbage collector is to monitor memory allocation and determine when a block of allocated memory
    is no longer needed and reclaim it. JavaScript automatically allocates memory when objects are created and
    frees it when they are not used anymore.

************************************************************************************************************************

|| What is destructuring?

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment

The destructuring assignment syntax is a JavaScript expression that makes it possible to unpack values from arrays,
    or properties from objects, into distinct (odlišný) variables.
The destructuring assignment uses similar syntax as the object and array literal expressions,
    but on the left-hand side of the assignment it defines what values to unpack from the sourced variable.

ARRAY destructuring
const x = [1, 2, 3, 4, 5];
const [y, z] = x;
console.log(y); // 1
console.log(z); // 2

const foo = ['one', 'two'];
const [red, yellow, green, blue] = foo;
console.log(red); // "one"
console.log(yellow); // "two"
console.log(green); // undefined
console.log(blue);  //undefined

Assignment separate from declaration:
let a, b;
[a, b] = [1, 2];
console.log(a); // 1
console.log(b); // 2

Default values:
let a, b;
[a=5, b=7] = [1];
console.log(a); // 1
console.log(b); // 7

Swapping variables:
let a = 1;
let b = 3;
[a, b] = [b, a];
console.log(a); // 3
console.log(b); // 1

const arr = [1,2,3];
[arr[2], arr[1]] = [arr[1], arr[2]];
console.log(arr); // [1,3,2]

Assigning the rest of an array to a variable:
const [a, ...b] = [1, 2, 3];
console.log(a); // 1
console.log(b); // [2, 3]

Parsing an array returned from a function:
function f() {
  return [1, 2];
}
let a, b;
[a, b] = f();
console.log(a); // 1
console.log(b); // 2

OBJECT destructuring
const user = {
    id: 42,
    isVerified: true
};
const {id, isVerified} = user;
console.log(id); // 42
console.log(isVerified); // true

Assignment separate from declaration:
let a, b;
({a, b} = {a: 1, b: 2});
NOTE: The parentheses ( ... ) around the assignment statement are required when using object literal destructuring
    assignment without a declaration, otherwise the left-hand side is considered a block and not an object literal.
'({a, b} = {a: 1, b: 2})' is valid, as is 'const {a, b} = {a: 1, b: 2}'

Assigning to new variable names:
const o = {p: 42, q: true};
const {p: foo, q: bar} = o;
console.log(foo); // 42
console.log(bar); // true

Default values:
const {a = 10, b = 5} = {a: 3};
console.log(a); // 3
console.log(b); // 5

const {a: aa = 10, b: bb = 5} = {a: 3};
console.log(aa); // 3
console.log(bb); // 5

Unpacking fields from objects passed as a function parameter:
const user = {
  id: 42,
  displayName: 'jdoe',
  fullName: {
    firstName: 'John',
    lastName: 'Doe'
  }
};
function userId({id}) {
  return id;
}
function whois({displayName, fullName: {firstName: name}}) {
  return `${displayName} is ${name}`;
}
console.log(userId(user)); // 42
console.log(whois(user));  // "jdoe is John"

Setting a function parameter's default value:
function drawChart({size = 'big', coords = {x: 0, y: 0}, radius = 25} = {}) {
  console.log(size, coords, radius);
  // do some chart drawing
}
drawChart({
  coords: {x: 18, y: 30},
  radius: 30
});

Rest in Object Destructuring:
let {a, b, ...rest} = {a: 10, b: 20, c: 30, d: 40}
a; // 10
b; // 20
rest; // { c: 30, d: 40 }

Computed object property names and destructuring:
NOTE: Computed object property names allows from version ES6 declare keys in object literals through bracket notation
let key = 'z';
let {[key]: foo} = {z: 'bar'};
console.log(foo); // "bar"

************************************************************************************************************************

|| What is a Temporal Dead Zone?

Temporal Dead Zone is a behaviour that occurs with variables declared using let and const keywords because there is
    no hoisting valid any more.
It is a behaviour where we try to access a variable before it is initialized.

************************************************************************************************************************

|| In the following code snippet can you please predict the output or If you get an error, please explain the error?

<!DOCTYPE html>
<html>
<body>
<h2> <strong> Sample: Software Testing Help</strong> </h2>
<p id="studentName"></p>

<script>
var studentName = "Sajeesh Sreeni"; // String 'Sajeesh Sreeni' stored in studentName
var studentName; // varaible is decalred again
document.getElementById("studentName").innerHTML =
"Redeclaring the varaible will not lose the value!.<br>"
+"Here the value in studentName is "+ studentName;

 // Redeclaring the varaible will not lose the value!.
 // Here the value in studentName is Sajeesh Sreeni

</script>
</body>
</html>

Answer: This code will not produce any errors. Redeclaration of the variables is allowed in JavaScript.
Hence, the value of the variable will not be lost after the execution of the statement here.

************************************************************************************************************************

|| What is the difference between test() and exec() methods?

Both test() and exec() are RegExp  expression methods.
'RegExp' is built-in object used for matching text with a pattern.

By using a test(), we will search a string for a given pattern, if it finds the matching text then it returns
    the Boolean value ‘true’ or else it returns ‘false’.

But in exec(), we will search a string for a given pattern, if it finds the matching text then it returns
    the pattern itself or else it returns ‘null’ value.

************************************************************************************************************************

|| Have you used any browser for debugging? If yes, how is it done?

YES!
By pressing the ‘F12’ key in the keyboard we can enable debugging in the browser. Chose the ‘Console’ tab to view the results.
In Console, we can set breakpoints and view the value in variables.
All modern browsers have a built-in debugger with them (For example: Chrome, Firefox, Opera, and Safari).
This feature can be turned ON and OFF.

************************************************************************************************************************

|| What is the use of the ‘debugger’ keyword in JavaScript code?

Using the ‘debugger’ keyword in the code is like using breakpoints in the debugger.

************************************************************************************************************************

|| What are the distinct types of Error Name Values?

SER TU(R) :)

There are 6 types of values in ‘Error Name’ Property.

Range Error
    We will get this error if we use a number outside the range
Syntax Error
    This error raises when we use the incorrect syntax. (Please refer Ques No: 7)
Reference Error
    This error is thrown if used an undeclared variable Please refer Ques No: 19
Eval Error
    Thrown due to the error in eval(). New JavaScript version doesn’t have this error
Type Error
    Value is outside the range of types used. Please refer Ques No :22
URI Error
    Due to the usage of illegal characters.

************************************************************************************************************************

|| How to split a string into array items?

A string can be split into an array using the JavaScript split() method. This method takes a single parameter,
    the character you want to separate the string at, and returns the substrings between the separator as items in an array.

************************************************************************************************************************

|| How to join array items into a string?

Array items can be joined using the join() method.

************************************************************************************************************************

|| How to handle a large number of choices for one condition in an effective way?

This is done using switch statements.

************************************************************************************************************************

|| What is a ternary operator?

The conditional (ternary) operator is the only JS operator that takes three operands:
    a condition followed by a question mark (?),
    then an expression to execute if the condition is truthy followed by a colon (:),
    and finally the expression to execute if the condition is falsy.
This operator is frequently used as a shortcut for the if statement.

Conditional chains
The ternary operator is right-associative, which means it can be "chained" in the following way,
    similar to an if … else if … else if … else chain:
function example(…) {
    return condition1 ? value1
         : condition2 ? value2
         : condition3 ? value3
         : value4;
}

// Equivalent to:

function example(…) {
    if (condition1) { return value1; }
    else if (condition2) { return value2; }
    else if (condition3) { return value3; }
    else { return value4; }
}

************************************************************************************************************************

|| What is a potential pitfall with using 'typeof bar === "object"' to determine if bar is an object?
    How can this pitfall be avoided?

'null' is also considered an object so return is true if 'let bar = null'.
To avoid this we check:
(bar !== null) && (typeof bar === "object")

Also there could be issue with arrays and functions:
- as functions are considered an function but we want to include them as true for above checking, we have to include
    this type of:
    (bar !== null) && ((typeof bar === "object") || (typeof bar === "function"))
- as array is considered an object but we want to return true only for real objects (not all objects data type), we have to
    exclude them. If we do not want functions we could use constructor property for checking:
    (bar !== null) && (bar.constructor === Object)
    => it returns 'false' for nulls, arrays, and functions, but 'true' for objects.

************************************************************************************************************************

|| What will the code below output to the console and why?*/
    (function()
        { var a = b = 3; }
    )();

    console.log("a defined? " + (typeof a !== 'undefined'));
    console.log("b defined? " + (typeof b !== 'undefined'));
/*
output:
    a defined? false
    b defined? true

WHY?
Zapis je shorthandem tohoto:
    b = 3;
    var a = b;
Pokud nepouzijeme 'use strict', tak 'b' je bez uziti keyword cteno jako globalni promenna - je hoistovano mimo function
    scope. 'a' je tedy potom sice inicializovano, ale neni k nemu pristup mimo rozsah funkce.
    I kdybychom pouzili pro 'a' keyword let, stejne je 'b' cteno bez keywordu!!! Stejny vysledek.

************************************************************************************************************************

|| What will the code below output to the console and why?
*/
var myObject = {
    foo: "bar",
    func: function() {
        var self = this;
        console.log("outer func: this.foo = " + this.foo);
        console.log("outer func: self.foo = " + self.foo);
        (function() {
            console.log("inner func: this.foo = " + this.foo);
            console.log("inner func: self.foo = " + self.foo);
        })();
    }
};

myObject.func();
/*
output:
outer func:  this.foo = bar
outer func:  self.foo = bar
inner func:  this.foo = undefined
inner func:  self.foo = bar

WHY?
V ramci fce scope pro metodu 'func' 'this' i promenna 'self' odkazuje na objekt 'myObject'.
Ale v ramci rozsahu vnitrni IIFE fce odkazuje 'this' na obalovaci fci a je undefined, ptze obalovaci fce
    nema nadefinovano nic jako 'this.foo'.
    Oproti tomu diky lexical scopu stale dokaze pristoupit k promenne 'self' deklarovane v obalovaci fci a dokaze
    tedy vratit hodnotu 'bar'.

************************************************************************************************************************

|| What is the significance of, and reason for, wrapping the entire content of a JavaScript source file in a function block?

This is an increasingly common practice, employed by many popular JavaScript libraries (jQuery, Node.js, etc.).
This technique creates a closure around the entire contents of the file which creates a private namespace
    and thereby helps avoid potential name clashes between different JavaScript modules and libraries.
Another feature of this technique is to allow for an easily referencable (presumably shorter) alias
    for a global variable. This is often used, for example, in jQuery plugins.

************************************************************************************************************************

|| What will the code below output? Explain your answer.
*/
console.log(0.1 + 0.2);
console.log(0.1 + 0.2 == 0.3);
/*
You can’t be sure. it might print out 0.3 and true, or it might not.
Numbers in JavaScript are all treated with floating point precision, and as such, may not always yield
    the expected results.”

output:
0.30000000000000004
false

SOLUTION: */
    function areTheNumbersAlmostEqual(num1, num2) {
        return Math.abs( num1 - num2 ) < Number.EPSILON;
    }
    console.log(areTheNumbersAlmostEqual(0.1 + 0.2, 0.3));
/*
************************************************************************************************************************

|| In what order will the numbers 1-4 be logged to the console when the code below is executed? Why?
*/
(function() {
    console.log(1);
    setTimeout(function(){
        console.log(2)}, 1000);
    setTimeout(function(){
        console.log(3)}, 0);
    console.log(4);
})();
/*
output:
1
4
3
2

WHY?
Because of JavaScript events and timing, other words how js engine parse the code.

JS ENGINE:
- takes and executes the code, napr. Google V8 engine (Chrome), if everything is correct the parser produces
a data structure, known as the Abstract Syntax Tree, which is then translated into machine code

EXECUTION CONTEXT:
- all JavaScript code needs to run in an environment, and these environments are called execution contexts
- muzeme si to predstavit jako box nebo container
- jako prvni vytvari global objekty, pak local objekty a poradi, ve kterem si boxy stavi, se nazyva:
EXECUTION STACK - nekdy nazyvano jako CALL STACK
EXECUTION STACK vs. SCOPE CHAIN
    EXECUTION STACK = poradi, ve kterem jsou objekty v kodu volany
    SCOPE CHAIN = poradi, ve kterem jsou objekty v kodu napsane

V JS enginu se vytvari krome exec.stacku i tzv.
MESSAGE QUEUE (nekdy nazyvano jako CALLBACK QUEUE), sem jsou vkladany vsechny eventy, ktere se v prohlizeci
    stanou; jsou zde a cekaji na zprocesovani, dokud nejsou vyrizeny vsechny "zalezitosti" v exec.stacku;
    az jsou vyrizeny "zalezitosti" v exec.stacku, pak jsou do exec.stacku "presunuty" i udalosti, resp. fce napojene na eventy

EXECUTION CONTEXT with OBJECT - 3 vlastnosti:
    1.variable object (variable, fce declaration a fce expressions),
    2.scope chain
    3."this" variable

CREATION PHASE
    (variable object creation, scope chain creation a "this" variable is determined and set)
and
EXECUTION PHASE

************************************************************************************************************************

|| Write a simple function (less than 160 characters) that returns a boolean indicating whether or not
    a string is a palindrome.
*/
function isPalindrome(str) {
    str = str.replace(/\W/g, '').toLowerCase();
    return (str == str.split('').reverse().join(''));
}
/*
- nejprve musime nahradit nezadouci znaky pomoci regEx vyrazu: /\W/g a prevedeme vsechna pismena na mala
- muzeme vyuzit built-in metody split, reverse a join

- pokud nechceme prilis pouzivat built-in metody, muzeme provest reversi pomoci for loop:
*/
function palindrome(str) {
    let re = /[^A-Za-z0-9]/g;
    str = str.toLowerCase().replace(re, '');
    let len = str.length;
    for (let i = 0; i < len/2; i++) {
        if (str[i] !== str[len - 1 - i]) {
            return false;
        }
    }
    return true;
}
/*
- vice reseni na palindrome zde:
https://www.freecodecamp.org/news/two-ways-to-check-for-palindromes-in-javascript-64fea8191fd7/

************************************************************************************************************************

|| Write a sum method which will work properly when invoked using either syntax:
    console.log(sum(2,3));
    or
    console.log(sum(2)(3));

When a function is invoked, JavaScript does not require the number of arguments to match the number of arguments
    in the function definition. If the number of arguments passed exceeds the number of arguments in the function
    definition, the excess arguments will simply be ignored. On the other hand, if the number of arguments passed
    is less than the number of arguments in the function definition, the missing arguments will have a value
    of undefined when referenced within the function:
*/
function sum(x, y) {
    if (y !== undefined) {
        return x + y;
    } else {
        return function(y) { return x + y; };
    }
}
/*
Or we can use 'arguments' object which provides access to the actual arguments passed to a function:*/
function sum2(x) {
    if (arguments.length == 2) {
        return arguments[0] + arguments[1];
    } else {
        return function(y) { return x + y; };
    }
}
/*
************************************************************************************************************************

|| Consider the following code snippet:*/
for (var i = 0; i < 5; i++) {
    var btn = document.createElement('button');
    btn.appendChild(document.createTextNode('Button ' + i));
    btn.addEventListener('click', function(){ console.log(i); });
    document.body.appendChild(btn);
}
/*
(a) What gets logged to the console when the user clicks on “Button 4” and why?

No matter what button the user clicks the number '5' will always be logged to the console.
This is because, at the point that the onclick method is invoked (for any of the buttons), the for loop has already
    completed and the variable i already has a value of 5.

(b) Provide one or more alternate implementations that will work as expected.

The simplest solution, if you’re in an ES6/ES2015 context, is to use let i instead of var i:*/
for (let i = 0; i < 5; i++) {
  var btn = document.createElement('button');
  btn.appendChild(document.createTextNode('Button ' + i));
  btn.addEventListener('click', function(){ console.log(i); });
  document.body.appendChild(btn);
}
/* In ES5 we can capture the value of i at each pass through the for loop by passing it into a newly created function object.*/
for (var i = 0; i < 5; i++) {
  var btn = document.createElement('button');
  btn.appendChild(document.createTextNode('Button ' + i));
  btn.addEventListener('click', (function(i) {
    return function() { console.log(i); };
  })(i));
  document.body.appendChild(btn);
}
/* OR */

for (var i = 0; i < 5; i++) {
  var btn = document.createElement('button');
  btn.appendChild(document.createTextNode('Button ' + i));
  (function (i) {
    btn.addEventListener('click', function() { console.log(i); });
  })(i);
  document.body.appendChild(btn);
}
/*
************************************************************************************************************************

|| Assuming d is an “empty” object in scope, say: var d = {};
    What is accomplished using the following code? */

[ 'zebra', 'horse' ].forEach(function(k) { d[k] = undefined; });
/*
output:
{zebra: undefined, horse: undefined}

This is a useful strategy for ensuring that an object has a given set of properties.
Passing this object to Object.keys will return an array with those set keys as well (even if their values are undefined).

************************************************************************************************************************

|| What will the code below output to the console and why?
*/
var arr1 = "john".split(''); // arr1 = ['j', 'o', 'h', 'n']
var arr2 = arr1.reverse(); // arr2 = ['n', 'h', 'o', 'j']; arr1 = ['n', 'h', 'o', 'j']
var arr3 = "jones".split(''); // arr3 = ['j', 'o', 'n', 'e', 's']
arr2.push(arr3); // arr1 = arr2 = ['n', 'h', 'o', 'j',  ['j', 'o', 'n', 'e', 's']]
console.log("array 1: length=" + arr1.length + " last=" + arr1.slice(-1));
console.log("array 2: length=" + arr2.length + " last=" + arr2.slice(-1));

/*
output:
array 1: length=5 last=j,o,n,e,s
array 2: length=5 last=j,o,n,e,s

WHY?
Calling an array object’s reverse() method doesn’t only return the array in reverse order,
    it also reverses the order of the array itself (i.e., in this case, arr1).
The reverse() method returns a reference to the array itself (i.e., in this case, arr1).
    As a result, arr2 is simply a reference to (rather than a copy of) arr1.
    Therefore, when anything is done to arr2 (i.e., when we invoke arr2.push(arr3);),
        arr1 will be affected as well since arr1 and arr2 are simply references to the same object.

************************************************************************************************************************

||What will the code below output to the console and why?

console.log(1 + "2" + "2");
console.log(1 + +"2" + "2");
console.log(1 + -"1" + "2");
console.log(+"1" + "1" + "2");
console.log( "A" - "B" + "2");
console.log( "A" - "B" + 2);

output:
"122"
"32"
"02"
"112"
"NaN2"
NaN

WHY?
1. The first operation to be performed in 1 + "2". Since one of the operands ("2") is a string, JavaScript assumes
    it needs to perform string concatenation and therefore converts the type of 1 to "1".
2. Based on order of operations, the first operation to be performed is +"2" (the extra + before the first "2" is
    treated as a unary operator).
    Thus, JavaScript converts the type of "2" to numeric and then applies the unary + sign to it.
    As a result, the next operation is now 1 + 2 which of course yields 3. Then it continue as above example.
3. The explanation here is identical to the prior example, except the unary operator is - rather than +.
4. Although the first "1" operand is typecast to a numeric value based on the unary + operator that precedes it,
    it is then immediately converted back to a string when it is concatenated with the second "1" operand.
5. Since the - operator can not be applied to strings, and since neither "A" nor "B" can be converted to numeric values,
    "A" - "B" yields NaN which is then concatenated with the string "2".
6. As explained in the previous example, "A" - "B" yields NaN. But any operator applied to NaN with any other numeric
    operand will still yield NaN.

POZN.
unární operace taková operace, která má jediný operand. V programování např.
x++
-x
!x

************************************************************************************************************************

|| The following recursive code will cause a stack overflow if the array list is too large.
    How can you fix this and still retain the recursive pattern? */
var list = readHugeList();
var nextListItem = function() {
    var item = list.pop();
    if (item) {
        // process the list item...
        nextListItem();
    }
};
/*
SOLUTION:
The stack overflow is eliminated because the event loop handles the recursion, not the call stack.
When 'nextListItem' runs, if item is not null, the timeout function (nextListItem) is pushed to the event queue
    and the function exits, thereby leaving the call stack clear.
When the event queue runs its timed-out event, the next item is processed and a timer is set to again invoke nextListItem.
Accordingly, the method is processed from start to finish without a direct recursive call, so the call stack remains
    clear, regardless of the number of iterations:
*/
var list = readHugeList();

var nextListItem = function() {
    var item = list.pop();

    if (item) {
        // process the list item...
        setTimeout( nextListItem, 0);
    }
};
/*
************************************************************************************************************************

|| What will be the output when the following code is executed? Explain. */

console.log(false == '0');
console.log(false === '0');
/*
output:
true
false

WHY?
1. In JavaScript “0” is equal to false because “0” is of type string but when it tested for equality
    the automatic type conversion of JavaScript comes into effect and converts the “0” to its numeric value
    which is 0 and as we know 0 represents false value. So, “0” equals to false.
2. because of how '==' and '===' operators work.

************************************************************************************************************************

|| What is the output out of the following code? Explain your answer.*/

var a={}, b={key:'b'}, c={key:'c'};
a[b]=123;
a[c]=456;
console.log(a[b]);
/*
output: 456

WHY?
When setting an object property, JavaScript will implicitly stringify the parameter value. As keys can be only strings
    or symbols.
In this case, since b and c are both objects, they will both be converted to "[object Object]".
As a result, a[b] and a[c] are both equivalent to a["[object Object]"] and can be used interchangeably.
Therefore, setting or referencing a[c] is precisely the same as setting or referencing a[b].

************************************************************************************************************************

|| What will the following code output to the console. Explain your answer.*/

console.log((function f(n){
    return ((n > 1) ? n * f(n-1) : n)
})(10));
/*
The code will output the value of 10 factorial (i.e., 10!, or 3,628,800).
The named function f() calls itself recursively, until it gets down to calling f(1) which simply returns 1.

************************************************************************************************************************

|| Consider the code snippet below. What will the console output be and why?*/

(function(x) {
    return (function(y)
    { console.log(x); }
    )(2)
})(1);
/*
output: 1

WHY?
Because of a closure / lexical scoping.
In this example, since x is not defined in the inner function, the scope of the outer function is searched for
    a defined variable x, which is found to have a value of 1.

************************************************************************************************************************

|| What will the following code output to the console and why?
    What is the issue with this code and how can it be fixed? */
var hero = {
    _name: 'John Doe',
    getSecretIdentity: function (){
        return this._name; }
};
var stoleSecretIdentity = hero.getSecretIdentity;
console.log(stoleSecretIdentity());
console.log(hero.getSecretIdentity());
/*
output: 1
undefined
John Doe

WHY?
The first console.log prints undefined because we are extracting the method from the hero object,
    so stoleSecretIdentity() is being invoked in the global context (i.e., the window object) where
        the _name property does not exist.

SOLUTION:
using 'bind' method */
var hero = {
    _name: 'John Doe',
    getSecretIdentity: function (){
        return this._name; }
};
var stoleSecretIdentity = hero.getSecretIdentity.bind(hero);
console.log(stoleSecretIdentity());
console.log(hero.getSecretIdentity());
/*
************************************************************************************************************************

|| Create a function that, given a DOM Element on the page, will visit the element itself and all of its descendents
   (not just its immediate children). For each element visited, the function should pass that element to a provided
   callback function. The arguments to the function should be:
        a DOM element
        a callback function (that takes a DOM element as its argument)

Visiting all elements in a tree (DOM) is a classic "Depth-First-Search algorithm" application.
Here’s an example solution:*/
function Traverse(p_element,p_callback) {
    p_callback(p_element);
    var list = p_element.children;
    for (var i = 0; i < list.length; i++) {
        Traverse(list[i],p_callback);  // recursive call
    }
}
/*
************************************************************************************************************************

|| What is the output of the following code? */
var length = 10;
function petr() {
    console.log(this.length);
}
var obj = {
    length: 5,
    method: function(fn) {
        fn();
        arguments[0]();
    }
};
obj.method(petr, 1);
/*
10
2

WHY?
In the first place, as fn is passed as a parameter to the function method, the scope (this) of the function fn
    is window. (var length = 10; is declared at the window level).
    It also can be accessed as window.length or length or this.length (when this === window.)
'method' is bound to Object obj, and obj.method is called with parameters fn and 1.
    Though method is accepting only one parameter, while invoking it has passed two parameters;
    the first is a function callback and other is just a number.
When fn() is called inside method, which was passed the function as a parameter at the global level,
    this.length will have access to var length = 10 (declared globally) not length = 5 as defined in Object obj.
Now, we know that we can access any number of arguments in a JavaScript function using the arguments[] array.
    Hence arguments[0]() is nothing but calling fn(). Inside fn now, the scope of 'this' function becomes
        the arguments array, and logging the length of arguments[] will return 2.

************************************************************************************************************************

|| Consider the following code. What will the output be, and why? */
(function () {
    try {
        throw new Error();
    } catch (x) {
        var x = 1, y = 2;
        console.log('x within catch st: '+x);
    }
    console.log('x outside catch st: '+x);
    console.log('y outside catch st: '+y);
})();
/*
x within catch st: 1
x outside catch st: undefined
y outside catch st: 2

WHY?
var statements are hoisted (without their value initialization) to the top of the global or function scope it belongs to,
    even when it’s inside a with or catch block.
    In this case
However, the error’s identifier is only visible inside the catch block. It is equivalent to:

(function () {
    var x, y; // outer and hoisted
    try {
        throw new Error();
    } catch (x) { // inner
    x = 1; // inner x, not the outer one
    y = 2; // there is only one y, which is in the outer scope
    console.log(x); // inner
}
console.log(x);
console.log(y);
})();

************************************************************************************************************************

|| What will be the output of this code? */
var x = 21;

var girl = function () {
    console.log(x);
    var x = 20;
};

girl();
/*
undefined

WHY?
It’s because JavaScript initialization is not hoisted.
(Why doesn’t it show the global value of 21? The reason is that when the function is executed, it checks that
    there’s a local x variable present but doesn’t yet declare it, so it won’t look for global one.)

************************************************************************************************************************

|| What will this code print? */
for (let i = 0; i < 5; i++) {
    setTimeout(function() {
        console.log(i);
    }, i * 1000 );
}
/*
0
1
2
3
4

WHY?
Because we use let instead of var here. The variable i is only seen in the for loop’s block scope.

************************************************************************************************************************

|| What do the following lines output, and why? */
console.log(1 < 2 < 3);
console.log(3 > 2 > 1);
/*
true
false

WHY?
The 2nd returns 'false' because of how the engine works regarding operator associativity for < and >.
It compares left to right, so 3 > 2 > 1 JavaScript translates to true > 1.
true has value 1, so it then compares 1 > 1, which is false.

************************************************************************************************************************

|| How do you add an element at the begining of an array and one at the end with using ES6 possibilities?

myArray = ['start', ...myArray];
myArray = [...myArray, 'end'];
OR in short:
myArray = ['start', ...myArray, 'end'];

************************************************************************************************************************

|| Will adding a[10] result in a crash? What will the output of console.log be? */
var a = [1, 2, 3]
a[10] = 99;
console.log(a[6]);
/*

It will not crash. The JavaScript engine will make array slots 3 through 9 be “empty slots.”:
    [1, 2, 3, empty × 7, 99]

Here, a[6] will output 'undefined', but the slot still remains empty rather than filled with undefined.
This may be an important nuance in some cases. For example, when using map(), empty slots will remain empty
    in map()’s output, but undefined slots will be remapped using the function passed to it:
        var b = [undefined];
        b[2] = 1;
        console.log(b);             // (3) [undefined, empty × 1, 1]
        console.log(b.map(e => 7)); // (3) [7,         empty × 1, 7]

************************************************************************************************************************

|| What is the value of: typeof undefined == typeof NULL?

The expression will be evaluated to 'true', since NULL will be treated as any other undefined variable.
Note: JavaScript is case-sensitive and here we are using NULL instead of null!!!!

************************************************************************************************************************

|| What would following code return? */
console.log(typeof typeof 1);
/*
string

typeof 1 will return "number" and typeof "number" will return string.

************************************************************************************************************************

|| What will be the output of the following code.
    Explain your answer. How could the use of closures help here? */

for (var i = 0; i < 5; i++) {
    setTimeout(function() {
        console.log(i);
    }, i * 1000 );
}
/*
The code sample shown will not display the values 0, 1, 2, 3, and 4 as might be expected;
    rather, it will display 5, 5, 5, 5, and 5.
The reason for this is that each function executed within the loop will be executed after the entire loop has completed
    and all will therefore reference the last value stored in i, which was 5.
Closures can be used to prevent this problem by creating a unique scope for each iteration,
    storing each unique value of the variable within its scope, as follows: */
for (var i = 0; i < 5; i++) {
    (function(x) {
        setTimeout(function() { console.log(x); }, x * 1000 );
    })(i);
}
/*
************************************************************************************************************************

|| What will the following code output and why? */

var b = 1;

function outer(){
    var b = 2;
    function inner(){
        b++;
        var b = 3;
        console.log(b);
    }
    inner();
}

outer();

/*
3

WHY?
There are three closures in the example, each with it’s own var b declaration.
When a variable is invoked closures will be checked in order from local to global until an instance is found.
Since the inner closure has a b variable of its own, that is what will be output.

Zajimavy output pokud umistime console.log(b) pred var b = 3:
    NaN
        Ptze, b je hoistovano (ale pouze deklarece ne
        inicializace), a pokud se snazi provest matematickou operaci b++, nemuze ji provest na promenne, ktera je
        undefined.

************************************************************************************************************************

|| Discuss possible ways to write a function isInteger(x) that determines if x is an integer.

with ES6:
    using Number.isInteger() function
prior ES6 - with use of the bitwise XOR operator ( ^ ):
    function isInteger(x) { return (x ^ 0) === x; }
The following solution would also work, although not as elegant as the one above:
function isInteger(x) { return (typeof x === 'number') && (x % 1 === 0); }

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#bitwise_operators

************************************************************************************************************************

|| How do you clone an object? */

var obj = {a: 1 ,b: 2}
var objclone = Object.assign({},obj);
/*
Now the value of 'objclone' is {a: 1 ,b: 2} but points to a different object than 'obj'.

Note the potential pitfall, though: Object.assign() will just do a shallow copy, not a deep copy.
This means that nested objects aren’t copied. They still refer to the same nested objects as the original:

let obj = {
    a: 1,
    b: 2,
    c: {
        age: 30
    }
};

var objclone = Object.assign({},obj);
console.log('objclone: ', objclone);

obj.c.age = 45;
console.log('After Change - obj: ', obj);           // 45 - This also changes
console.log('After Change - objclone: ', objclone); // 45

************************************************************************************************************************

|| What is difference between i++ and ++i / i-- and --i?


The difference between i++ and ++i is the value of the expression.

The value i++ is the value of i before the increment.
The value of ++i is the value of i after the increment.

Example:

var i = 42;
alert(i++); // shows 42
alert(i); // shows 43
i = 42;
alert(++i); // shows 43
alert(i); // shows 43


++variable increments the variable, returning the new value.

variable++ increments the variable, but returns the old value.

--variable decrements the variable, returning the new value.

variable-- decrements the variable, but returns the old value.

For example:

a = 5;
b = 5;
c = ++a; // c is 6
d = b++; // d is 5

************************************************************************************************************************






























*/


